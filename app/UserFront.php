<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserFront extends Model
{
    //
    protected $connection = 'mysql_frontend';
    protected $table = 'frontend_register';

    protected $fillable = [
        'uuid', 'name', 'username', 'email'
    ];

    public function location(){
        return $this->belongsTo('App\Location', 'city_id', 'id');
    }

    public function profession(){
        return $this->belongsTo('App\Profession', 'profession_id', 'uuid');
    }

    public function userQuiz(){
        return $this->hasMany('App\UserQuiz', 'user_id', 'user_id');
    }

}
