<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AvaliacaoItem extends Model
{
    //
    protected $connection = 'mysql';
    protected $table = 'avaliacao_item';
}
