<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    //
    protected $connection = 'mysql_frontend';
    protected $table = 'quiz_question';
}
