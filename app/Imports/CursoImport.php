<?php

namespace App\Imports;

use App\Curso;
use Maatwebsite\Excel\Concerns\ToModel;

class CursoImport implements ToModel
{
    
    public function model(array $row)
    {
        return new Curso([
            'id'     => $row[0],
            'display_name'    => $row[1], 
            'start' => $row[2], 
            'end' => $row[3] 
        ]);
    }
}
