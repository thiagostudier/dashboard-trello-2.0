<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    //
    protected $connection = 'mysql_edx';
    protected $table = 'auth_user';

    protected $fillable = [
        'id', 'username', 'email', 'is_staff', 'created'
    ];

    public function inscricaos(){
        return $this->hasMany('App\Inscricao', 'id', 'user_id');
    }

}
