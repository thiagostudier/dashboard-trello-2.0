<?php

namespace App\Http\Controllers;
use App\TrelloResources;
use Carbon\Carbon;
use Session;

use Illuminate\Http\Request;

class TrelloController extends Controller
{
    //
    public function index(){

        $token = Session::get(env('TOKEN_TRELLO', 'token')); //PEGAR TOKEN DE VALIDAÇÃO
        $idMember = Session::get(env('ID_MEMBER_TRELLO', 'id_member')); //PEGAR ID DO MEMBRO
        $from = Session::get('from'); //PEGAR DATA DE COMEÇO
        $to = Session::get('to'); //PEGAR DATA DE TÉRMINO
        
        $boards = TrelloResources::getBoards($token, $idMember); //PEGAR QUADROS
        $board_default = TrelloResources::getBoardDefault($token, $idMember); //PEGAR QUADRO DEFAULT
        
        $list_end = Session::get('list_end_'.$board_default) ?? null;
        $list_not_count = Session::get('list_not_count_'.$board_default) ?? array(null);
        
        $member = TrelloResources::getMember($token, $idMember); //PEGAR MEMBRO 

        $cards = TrelloResources::getCards($board_default, $token); //PEGAR CARDS DO QUADRO
        $cards = collect($cards)->sortByDesc('due')->values()->all(); //ORDENAR POR DATA DA FINALIZAÇÃO DA ATIVIDADE

        $lists = TrelloResources::getLists($board_default, $token); //PEGAR LISTAS DO QUADRO
        $labels = TrelloResources::getLabels($board_default, $token); //PEGAR LABELS DO QUADRO 
        $members = collect(TrelloResources::getMembers($board_default, $token)); //PEGAR MEMBROS DO QUADRO        

        return view('trello.index', compact('token', 'boards', 'cards','labels','lists', 'member','members', 'board_default', 'to', 'from', 'list_end', 'list_not_count'));
    
    }

    public function user($id){

        $token = Session::get(env('TOKEN_TRELLO', 'token'));
        $idMember = Session::get(env('ID_MEMBER_TRELLO', 'id_member'));
        $from = Session::get('from');
        $to = Session::get('to');

        $boards = TrelloResources::getBoards($token, $idMember); //PEGAR QUADROS
        $board_default = TrelloResources::getBoardDefault($token, $idMember); //PEGAR QUADRO DEFAULT
        
        $list_end = Session::get('list_end_'.$board_default) ?? null;
        $list_not_count = Session::get('list_not_count_'.$board_default) ?? array(null);

        $member = TrelloResources::getMember($token, $idMember); //PEGAR MEMBRO 

        $cards = TrelloResources::getCards($board_default, $token); //PEGAR CARDS DO QUADRO
        $cards = collect($cards)->sortByDesc('due')->values()->all(); //ORDENAR POR DATA DA FINALIZAÇÃO DA ATIVIDADE
        
        $lists = TrelloResources::getLists($board_default, $token); //PEGAR LISTAS DO QUADRO
        $labels = TrelloResources::getLabels($board_default, $token); //PEGAR LABELS DO QUADRO 
        $members = collect(TrelloResources::getMembers($board_default, $token)); //PEGAR MEMBROS DO QUADRO
        
        $member_filter = TrelloResources::getMember($token, $id); //PEGAR USUÁRIO SELECIONADO
        
        return view('trello.user.index', compact('token', 'member_filter', 'boards', 'cards','labels','lists', 'member','members', 'board_default', 'to', 'from', 'list_end', 'list_not_count'));
        
    }

    public function card($id){

        $token = Session::get(env('TOKEN_TRELLO', 'token'));
        $idMember = Session::get(env('ID_MEMBER_TRELLO', 'id_member'));
        $from = Session::get('from');
        $to = Session::get('to');

        $boards = TrelloResources::getBoards($token, $idMember); //PEGAR QUADROS
        $board_default = TrelloResources::getBoardDefault($token, $idMember); //PEGAR QUADRO DEFAULT
        
        $list_end = Session::get('list_end_'.$board_default) ?? null;
        $list_not_count = Session::get('list_not_count_'.$board_default) ?? array(null);

        $member = TrelloResources::getMember($token, $idMember); //PEGAR MEMBRO 

        $card = TrelloResources::getCard($id, $token); //PEGAR CARDS DO QUADRO

        $date_create = \Carbon\Carbon::parse(TrelloResources::getDateCreateCardDefault($card->id))->format('d/m/y'); //DATA DA ENTREGA
        $date_finish = ($card->due ? \Carbon\Carbon::parse($card->due)->format('d/m/y') : ''); //DATA DA ENTREGA
        $last_activy = ($card->dateLastActivity ? \Carbon\Carbon::parse($card->dateLastActivity)->format('d/m/y') : ''); //DATA DA ÚLTIMA ATIVIDADE
        
        $lists = TrelloResources::getLists($board_default, $token); //PEGAR LISTAS DO QUADRO
        $list = TrelloResources::getDataJson($lists, $card->idList); //PEGAR LISTA DO CARD
        $labels = $card->labels; //PEGAR ETIQUETAS DO CARD
        $actions = TrelloResources::getCardActions($card->id, $token); //PEGAR AÇÕES DO CARD
        $members = collect(TrelloResources::getMembers($board_default, $token)); //PEGAR MEMBROS DO QUADRO
        
        $time_on_lists = TrelloResources::getCardListsDesc($card->id, $token, $lists);

        $time_until_finish = TrelloResources::getTimeUntilFinish($card->id, $token, $list_end);
        
        $id_checklists = $card->idChecklists; //PEGAR CHECKLISTS
        $checklists = array(); //CRIAR ARRAY VAZIO PARA OS CHECKLISTS
        
        if(!is_null($id_checklists)){
            foreach($id_checklists as $id_checklist){
                $checklists[] = TrelloResources::getChecklist($id_checklist, $token);
            }
        }

        return view('trello.card.index', compact('date_create', 'date_finish', 'last_activy', 'boards', 'checklists', 'card', 'labels', 'lists', 'list', 'member','members', 'actions', 'time_on_lists', 'time_until_finish', 'board_default', 'to', 'from', 'list_end', 'list_not_count', 'token'));

    }

    public function autenticacao(){
        if(!TrelloResources::hasToken()){
            return view('trello.autenticacao');
        }else{
            return redirect()->route('trello');
        }
    }

    public function atualizarToken(){
        $input = request()->all();
        $token = $input['token'];

        TrelloResources::createToken($token);

        return response()->json($token);
    }

    public function atualizar_datas(){
        $input = request()->all();
        $to = $input['to'];
        $from = $input['from'];

        TrelloResources::createDateFromTo($from, $to);

        return response()->json($from);
    }

    public function atualizarBoard(){
        $input = request()->all();
        TrelloResources::createBoardDefault($input['id']);
        return redirect()->back();
    }

    public function definition_create(){
        $input = request()->all(); //PEGAR DADOS DO POST
        $token = Session::get(env('TOKEN_TRELLO', 'token')); //PEGAR TOKEN DE VALIDAÇÃO
        $idMember = Session::get(env('ID_MEMBER_TRELLO', 'id_member')); //PEGAR ID DO MEMBRO
        $board_default = TrelloResources::getBoardDefault($token, $idMember); //PEGAR QUADRO DEFAULT
        
        $array = !empty($input['list_not_count']) ? $input['list_not_count'] : null;

        Session::put('list_end_'.$board_default, $input['list_end'] ?? null , 1000000);
        session(['list_not_count_'.$board_default => $array]);

        return redirect()->back(); 
    }

    public function limparCache(){
        TrelloResources::clearCache();
        return redirect()->route('trello'); 
    }

}
