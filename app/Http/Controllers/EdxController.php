<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Curso;
use App\Inscricao;
use App\Certificado;
use App\Module;
use App\Users;
use App\AvaliacaoItem;
use App\Question;
use App\Projeto;
use Cache;
use DB;
use File;
use App\Exports\CursoExport;
use App\Exports\CursoExportTest;
use App\Imports\CursoImport;
use Maatwebsite\Excel\Facades\Excel;

class EdxController extends Controller
{

    //
    public function index(){

        $inscricaos = Inscricao::all();

        $courses = Curso::all();
        
        $inscricaos = Inscricao::
        whereNotIn(('student_courseenrollment.user_id'), function($q){
            $q->select('user_id')->from('student_courseaccessrole');
        })->count();
        
        $certificados_emitidos = 
        Certificado::whereNotIn(('certificates_generatedcertificate.user_id'), function($q){
            $q->select('user_id')->from('student_courseaccessrole');
        })->count();
        
        $page = "Indicadores EDX";

        return view('edx.index', compact('courses','inscricaos','certificados_emitidos', 'page'));

    }

    public function projetos(){
        
        $projetos = Projeto::all();

        $page = 'Projetos';
        
        return view('edx.projetos.index', compact('projetos', 'page'));

        // "uuid" => "1832426c25f34147bc620538deaeade4"
        // "created_at" => "2019-01-17 01:49:25"
        // "updated_at" => "2020-01-31 17:18:05"
        // "name" => "EAD ANVISA"
        // "is_visible" => 1
        // "order" => 10
        // "banner" => "projeto2.jpg"
        // "description" => """
        // <p>A Agência Nacional de Vigilância Sanitária (ANVISA) tem no seu escopo de atividades a orientação de como deve ser o funcionamento dos serviços de saúde. Send ▶
        // <p><b>Público Alvo:</b> Profissionais assistenciais e não-assistenciais que atuam com controle de infecções ou controle do uso de antimicrobianos ou profissiona ▶
        // """
        // "navigation" => 0
        // "category_id" => "e245dbd6b4e642eea0cfb9f73cd9d8d6"
        // "curriculum_id" => "c0b7564cc547418aadee9aee6001ccaa"
        // "slug" => "ead-anvisa"
        // "target_audience" => "<p></p>"
        // "presentation_banner" => "AF_HMV_ANVISA_BannersDigitais_Projeto_1200x270px.jpg"
        // "interest_record" => 0

    }

    public function projeto($id){

        $projeto = Projeto::where('uuid', $id)->first();

        $curriculos = $projeto->curriculos;
        $cursos = array();
        foreach($curriculos as $curriculo){
            foreach($curriculo->cursos as $curso){
                $cursos[] = $curso;
            }
        }
        
        collect($cursos);

        $page = $projeto->name;
        
        return view('edx.projetos.view', compact('projeto', 'cursos', 'page'));
    }

    public function curso($id){

        $curso = Curso::find($id); //PEGAR CURSO
        
        $page = $curso->display_name;
        
        // REMOVER CARACTERES ESPECIAIS
        function clean($string) {
            $string = str_replace(' ', '-', $string);
            return preg_replace('/[^A-Za-z0-9\-]/', '', $string);
        }

        $folder = 'cursos/'.clean($curso->id);

        $files = [];

        if(\Storage::disk('s3')->exists($folder)) {

            $filesInFolder = \Storage::disk('s3')->files($folder);

            foreach($filesInFolder as $path){
                $url = Curso::getLink($path);
                $files[] = [
                    'name' => pathinfo($path),
                    'url' =>  $url       
                ];
            }

        }

        // NÚMERO DE INSCRITOS
        $inscricaos = Inscricao::where('course_id', $id)
        ->whereNotIn(('student_courseenrollment.user_id'), function($q){
            $q->select('user_id')->from('student_courseaccessrole');
        })->count();
        
        // NÚMERO DE CERTIFICADOS EMITIDOS
        $certificados_emitidos = 
        Certificado::where('course_id', $id)
        ->whereNotIn(('certificates_generatedcertificate.user_id'), function($q){
            $q->select('user_id')->from('student_courseaccessrole');
        })->count();

        return view("edx.curso.view", compact('curso', 'page', 'files', 'inscricaos', 'certificados_emitidos'));

    }

}
