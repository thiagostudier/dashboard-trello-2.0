<?php

namespace App\Http\Middleware;
use App\TrelloResources;
use Closure;

class TrelloAutenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!TrelloResources::hasToken()){
            return redirect()->route('autenticacao_trello');
        }else{
            return $next($request);
        }
    }
}
