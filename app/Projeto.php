<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projeto extends Model
{
    //
    protected $connection = 'mysql_frontend';
    protected $table = 'frontend_project';

    public function curriculos(){
        return $this->hasMany('App\Curriculo', 'project_id', 'uuid');
    }

}
