<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Certificado extends Model
{
    //
    protected $connection = 'mysql_edx';
    protected $table = 'certificates_generatedcertificate';

    public $incrementing = false;

    protected $primaryKey = 'id';

    protected $fillable = ['course_id', 'grade', 'user_id'];
    
}
