<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Exports\CursoExport;
use App\Imports\CursoImport;
use Maatwebsite\Excel\Facades\Excel;

class MakeExcelCursoComplete implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $id;

    function __construct($id) {
        $this->id = $id;
    }

    public function handle()
    {
        
        $folder = str_replace(' ', '-', $this->id);
        $folder = preg_replace('/[^A-Za-z0-9\-]/', '', $this->id);
        
        $now = date("Y-m-d-h-m-i");
        Excel::store(new CursoExport($this->id), 'cursos/'.$folder.'/relatorio-'.$now.'.xlsx', 's3');
        
    }

}
