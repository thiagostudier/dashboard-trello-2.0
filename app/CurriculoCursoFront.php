<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CurriculoCursoFront extends Model
{
    //
    protected $connection = 'mysql_frontend';
    protected $table = 'frontend_curriculum_courses';
}
