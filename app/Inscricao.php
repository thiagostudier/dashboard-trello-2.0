<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inscricao extends Model
{
    //
    protected $connection = 'mysql_edx';
    protected $table = 'student_courseenrollment';

    protected $fillable = [
        'id', 'course_id', 'created', 'is_active', 'user_id'
    ];

    public function user(){
        return $this->belongsTo('App\Users', 'user_id', 'id');
    }

    public function userFront(){
        return $this->belongsTo('App\UserFront', 'user_id', 'user_id');
    }

}