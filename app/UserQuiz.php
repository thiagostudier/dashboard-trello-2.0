<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserQuiz extends Model
{
    //
    protected $connection = 'mysql_frontend';
    protected $table = 'quiz_userquiz';

    public function answer(){
        return $this->hasMany('App\Answer', 'user_quiz_id', 'uuid');
    }

}
