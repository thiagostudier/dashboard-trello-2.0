<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    //
    protected $connection = 'mysql_edx';
    protected $table = 'courseware_studentmodule';

    protected $fillable = [
        'id', 'course_id', 'module_id', 'module_type', 'user_id', 'grade', 'max_grade', 'student_id'
    ];    

}
