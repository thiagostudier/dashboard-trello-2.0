<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curriculo extends Model
{
    //
    protected $connection = 'mysql_frontend';
    protected $table = 'frontend_curriculum';

    public function cursos(){
        return $this->belongsToMany('App\CursoFront', 'frontend_curriculum_courses','curriculum_id','course_id', 'uuid', 'uuid');
    }
}
