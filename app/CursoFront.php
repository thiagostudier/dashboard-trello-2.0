<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CursoFront extends Model
{
    //
    protected $connection = 'mysql_frontend';
    protected $table = 'frontend_course';
}
