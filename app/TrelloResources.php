<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DateTime;
use Session;
use DateInterval;
use Artisan;

class TrelloResources extends Model
{
    //PEGAR ID MEMBRO
    public static function getIdMember($token){
        $url = "https://api.trello.com/1/tokens/$token/member?key=f173ab582acacc165addb2dee7c21220&token=$token";
        $data = json_decode(file_get_contents($url));
        return $data->id;
    }
    //PEGAR QUADROS
    public static function getBoards($token, $idMember){
        $url = "https://api.trello.com/1/members/$idMember/boards?fields=name,id&key=f173ab582acacc165addb2dee7c21220&token=$token";
        $data = json_decode(file_get_contents($url));
        return $data;
    }
    //PEGAR QUADRO DEFAULT DO CACHE
    public static function createBoardDefault($id){
        // $trello = new TrelloResources();
        // Cache::put('board_default', $id, 100000);
        // $board_default = Cache::get('board_default');
        // return $board_default;
        $trello = new TrelloResources();
        Session::put('board_default', $id, 100000);
        $board_default = Session::get('board_default');
        return $board_default;
    }
    //PEGAR QUADRO DEFAULT DO CACHE
    public static function getBoardDefault($token, $idMember){
        // if(Cache::get('board_default')){
        //     $board_default = Cache::get('board_default');
        // }else{
        //     $trello = new TrelloResources();
        //     Cache::put('board_default', $trello->getBoards($token, $idMember)[0]->id, 100000);
        //     $board_default = Cache::get('board_default');
        // }
        // return $board_default;
        if(Session::get('board_default')){
            $board_default = Session::get('board_default');
        }else{
            $trello = new TrelloResources();
            Session::put('board_default', $trello->getBoards($token, $idMember)[0]->id, 100000);
            $board_default = Session::get('board_default');
        }
        return $board_default;
    }
    // PEGAR QUADRO
    public static function getBoard($id, $token){
        $url = "https://api.trello.com/1/boards/$id?key=f173ab582acacc165addb2dee7c21220&token=$token";
        $data = json_decode(file_get_contents($url));
        return $data;
    }
    // PEGAR CARDS DO BOARD
    public static function getCards($id, $token){        
        $url = "https://api.trello.com/1/boards/$id/cards?fields=badges,actions,all,due,dueComplete,idMembers,dateLastActivity,name,id,labels,idLabels,idList,shortLink&key=f173ab582acacc165addb2dee7c21220&token=$token";
        $data = json_decode(file_get_contents($url));
        return $data;
    }
    // PEGAR MEMBER CARDS
    public static function getMemberCards($token, $idMember){
        $url = "https://api.trello.com/1/members/$idMember/boards?fields=due,dueComplete,idMembers,dateLastActivity,name,id,labels,idLabels,idList&key=f173ab582acacc165addb2dee7c21220&token=$token";
        $data = json_decode(file_get_contents($url));
        return $data;
    }
    // GET LISTS
    public static function getLists($id, $token){
        $url = "https://api.trello.com/1/board/$id/lists?fields=name,id&key=f173ab582acacc165addb2dee7c21220&token=$token";
        $data = json_decode(file_get_contents($url));
        return $data;
    }
    // GET LIST
    public static function getList($id, $token){
        $url = "https://api.trello.com/1/lists/$id?fields=name&key=f173ab582acacc165addb2dee7c21220&token=$token";
        $data = json_decode(file_get_contents($url));
        return $data;
    }
    //PEGAR CARD
    public static function getCard($id, $token){
        $url = "https://api.trello.com/1/cards/$id?&actions=true&list=true&key=f173ab582acacc165addb2dee7c21220&token=$token";
        $data = json_decode(file_get_contents($url));
        return $data;
    }
    //PEGAR AÇÕES DO CARD
    public static function getCardActions($id, $token){
        $url = "https://api.trello.com/1/cards/$id/actions?fields=all&key=f173ab582acacc165addb2dee7c21220&token=$token";
        $data = json_decode(file_get_contents($url));
        return $data;
    }
    //PEGAR DATA DE CRIAÇÃO DO CARD
    public static function getDateCard($id){
        return date('Y-m-d', hexdec( substr( $id , 0, 8 ) ) );
    }
    //PEGAR ETIQUETAS
    public static function getLabels($id, $token){
        $url = "https://api.trello.com/1/boards/$id/labels/?fields=id,name&key=f173ab582acacc165addb2dee7c21220&token=$token";
        $data = json_decode(file_get_contents($url));
        return $data;
    }
    //PEGAR MEMBRO
    public static function getMember($token, $idMember){
        $url = "https://api.trello.com/1/members/$idMember?fields=initials,fullName,id,avatarUrl&key=f173ab582acacc165addb2dee7c21220&token=$token";
        $data = json_decode(file_get_contents($url));
        return $data;
    }
    //PEGAR MEMBROS
    public static function getMembers($id, $token){
        $url = "https://api.trello.com/1/boards/$id/members/?fields=initials,id,avatarUrl,fullName&key=f173ab582acacc165addb2dee7c21220&token=$token";
        $data = json_decode(file_get_contents($url));
        return $data;
    }
    //PROCURAR NO ARRAY
    public static function getDataJson($array, $data){
        $name = "";
        foreach($array as $item){if($item->id == $data){$name = $item->name;}}
        return $name;
    }
    //PEGAR AÇÕES DE UM BOARD
    public static function getActions($id, $token){

        $actions = array();
        $before = null;

        do{
            $url = "https://api.trello.com/1/boards/$id/actions/?&filter=updateCard&fields=id,date,data&key=f173ab582acacc165addb2dee7c21220&token=$token&before=$before";
            $data = json_decode(file_get_contents($url));
            array_push($actions, $data);
            $before = $actions[0][0]->id;
        } while(count($data) == 100);
        
        // dd($actions);
        
        $data = collect($data)->sortBy('date')->values()->all();
        
        
        
        return $data;

    }

    //PEGAR CHECKLIST DE UM CARD
    public static function getChecklist($id, $token){
        $url = "https://api.trello.com/1/checklists/$id?fields=all&key=f173ab582acacc165addb2dee7c21220&token=$token";
        $data = json_decode(file_get_contents($url));
        return $data;
    }
    //VERIFICAR SE O CARD ESTÁ ATRASADO
    public static function cardIsLate($card){
        if( isset($card->due) and (date('Y-m-d') > $card->due) and !$card->dueComplete) {
            return 'is-late'; 
        }else if($card->dueComplete){
            return 'is-finish';
        }else{
            return '';
        }
    }
    public static function filterCardActions($actions, $id){
        $array = array();
        foreach($actions as $action){
            if(isset($action->data->card->id) && $action->data->card->id == $id){
                $array[] = $action;
            }
        }
        return $array;
    }

    public static function getDateCreateCard($id){
        return date('d/m/y H:i',hexdec(substr($id,0,8)));
    }

    public static function getDateCreateCardDefault($id){
        return date('Y-m-d',hexdec(substr($id,0,8)));
    }

    public static function getDateTimeCreateCard($id){
        return date('D/M/Y H:I:S',hexdec(substr($id,0,8)));
    }

    public static function createToken($token){

        $trello = new TrelloResources();
        Session::put(env('TOKEN_TRELLO', 'token'), $token, 1000000);
        Session::put(env('ID_MEMBER_TRELLO', 'id_member'),$trello->getIdMember($token), 1000000);

        $date = date('Y-m-d');
        $date = date_create($date);
        date_sub( $date ,date_interval_create_from_date_string("30 days"));
        $from = Session::put('from', date_format($date,"Y-m-d") , 1000000);            
        $to = Session::put('to', date('Y-m-d'), 1000000);
        
    }

    public static function hasToken(){
        if(Session::get(env('TOKEN_TRELLO', 'token')) && Session::get(env('ID_MEMBER_TRELLO', 'id_member'))){
            return true;
        }else{
            return false;
        }
    }

    public static function createDateFromTo($from, $to){
        $trello = new TrelloResources();
        Session::put('from', $from, 1000000);
        Session::put('to', $to, 1000000);
    }

    public static function clearCache(){
        Session::forget(env('TOKEN_TRELLO', 'token'));
        Session::forget(env('ID_MEMBER_TRELLO', 'id_member'));
        Session::forget('board_default');
        Session::forget('list_end');
        Session::forget('list_not_count');
        return true;
    } 

    public static function getTimeUntilFinish($id, $token, $list_end){ //FUNÇÃO OK

        $data = array(); //CRIAR ARRAY
        $card = TrelloResources::getCard($id, $token); //PEGAR CARDS DO QUADRO
        $actions = TrelloResources::getCardActionsSortByDate($id, $token); //PEGAR AS AÇÕES DO CARD PELO ORDENADO PELAS DATAS 
        $data['date_create'] = new DateTime( date('Y-m-d H:i:s',hexdec(substr($id,0,8))) ); //PEGAR DATA DE CRIAÇÃO DO CARD

        //PERCORRER AÇÕES DO CARD
        foreach($actions as $action){
            if(!isset($data['list'])){
                $data['list'] = $card->list; //PEGAR A LISTA FINAL
            }
            if(isset($action->data->listAfter)){ //SE A AÇÃO ATUALIZAÇÃO DE LISTA
                $data['list'] = $action->data->listAfter; //PEGAR A LISTA FINAL
                $data['date_end'] = new DateTime($action->date); //PEGAR A DATA DA ALTERAÇÃO
            }
        }

        if(empty($actions)){
            $data['list'] = $card->list; //PEGAR A LISTA FINAL
            $data['date_end'] = date('Y-m-d H:i:s',hexdec(substr($id,0,8))); //PEGAR A DATA DA ALTERAÇÃO
        }

        // SE A LISTA FOR A LISTA FINAL PEGAR A DATA
        if($data['list']->id == $list_end){
            $data['date_end'] = new DateTime($action->date);
        }else{
            $data['date_end'] = new DateTime(date('Y-m-d H:i:s')); //SE NÃO POSSUIR UMA DATE DE TÉRMINO, INSERIR A DATA ATUAL
        }
        // PEGAR INTERVALO ENTRE AS DATAS PARA CALCULAR O TEMPO QUE A DEMNADA LEVOU PARA SER CONCLUÍDA
        $data['interval'] = $data['date_create']->diff($data['date_end'])->format('%dd%Hh%Imin');

        return $data;

    }

    // ORDENAÇÃO DO TEMPO QUE FICOU EM CADA LISTA
    public static function getCardActionsSortByDate($id, $token){
        $url = "https://api.trello.com/1/card/$id/actions/?fields=all&key=f173ab582acacc165addb2dee7c21220&token=$token";
        $data = json_decode(file_get_contents($url));
        $data = collect($data)->sortBy('date')->values()->all();
        return $data;
    }

    //FUNÇÃO PARA CALCULAR O TEMPO QUE O CARD FICOU EM CADA LISTA - REFATORAR A FUNÇÃO ESTÁ BEM CONFUSA
    public static function getCardListsDesc($id, $token, $lists){

        $actions = TrelloResources::getCardActionsSortByDate($id, $token);
        
        $array1 = array();
        $array2 = array();

        $idMember = Session::get(env('ID_MEMBER_TRELLO', 'id_member')); //PEGAR ID DO MEMRBO
        $board_default = TrelloResources::getBoardDefault($token, $idMember); //PEGAR QUADRO DEFAULT
        $list_end = Session::get('list_end_'.$board_default) ?? null; //PEGAR LISTA FINAL DO QUADRO DEFAULT
        
        foreach($actions as $key=>$action){

            if ($key === array_key_first($actions)){
                $idList = $action->data->card->idList ?? $action->data->list->id;
                $list = TrelloResources::getList($idList, $token); //PEGAR LISTA DO CARD
                $date = date('Y-m-d H:i:s',hexdec(substr($id,0,8)));
                $array1[] = ['list'=>$list->name, 'time'=>$date];
            }

            if(isset($action->data->listAfter->name)){

                $i = count($array1);
                $list = $array1[$i - 1]['list'];

                if ( $list != $action->data->listAfter->name ){

                    $listAfter = $action->data->listAfter->name;
                    $idListAfter = $action->data->listAfter->id;
                    $listBefore = $action->data->listBefore->name;
                    $date = \Carbon\Carbon::parse($action->date)->format('Y-m-d H:i:s');                
                    $array1[] = ['list'=>$listAfter, 'id'=> $idListAfter,'time'=>$date];

                }
                
            }

        }    
        
        if(empty($actions)){
            $card = TrelloResources::getCard($id, $token); //PEGAR CARDS DO QUADRO
            $date = date('Y-m-d H:i:s',hexdec(substr($id,0,8)));
            $array1[] = ['list'=>$card->list->name, 'id'=> $card->list->id, 'time'=>$date];
        }

        foreach ($array1 as $key => $item) {

            $datetime1 = new DateTime($array1[$key]["time"]);
            
            if( !isset($array1[$key]['id']) or $array1[$key]['id'] != $list_end ){
                
                $datetime2 = isset($array1[$key+1]["time"]) ? new DateTime($array1[$key+1]["time"]) : new DateTime(now());

                if($datetime1->diff($datetime2)->format('%d') == 0){
                    $interval = $datetime1->diff($datetime2)->format('%Hh%Imin');
                }else{
                    $interval = $datetime1->diff($datetime2)->format('%dd%Hh%Imin');
                }

                $array1[$key]["interval"] = $interval; 
                $array1[$key]["date_create"] = $datetime1->format('d/m/y');
                $array1[$key]["date_end"] = $datetime2->format('d/m/y');  
                
            }else{
                $array1[$key]["interval"] = ""; 
                $array1[$key]["date_create"] = $datetime1->format('d/m/y');
                $array1[$key]["date_end"] = "até agora";  
            }

        }

        return $array1;
    }
    
}  
