<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Curso extends Model
{
    //
    protected $connection = 'mysql_edx';
    protected $table = 'course_overviews_courseoverview';

    public $incrementing = false;

    protected $primaryKey = 'id';

    protected $fillable = ['display_name', 'start', 'end', 'id'];

    public function inscricaos(){
        return \DB::connection('mysql_edx')
            ->table('student_courseenrollment')
            ->join('course_overviews_courseoverview', 'student_courseenrollment.course_id', '=', 'course_overviews_courseoverview.id')
            ->where('student_courseenrollment.course_id', $this->id)
            ->whereNotIn(('student_courseenrollment.user_id'), function($q){
                $q->select('user_id')->from('student_courseaccessrole');
            })->get();
    }

    public function certificados(){
        return \DB::connection('mysql_edx')
            ->table('certificates_generatedcertificate')
            ->join('course_overviews_courseoverview', 'certificates_generatedcertificate.course_id', '=', 'course_overviews_courseoverview.id')
            ->where('certificates_generatedcertificate.course_id', $this->id)
            ->whereNotIn(('certificates_generatedcertificate.user_id'), function($q){
                $q->select('user_id')->from('student_courseaccessrole');
            })->get();
    }
    
    public function modules(){
        return $this->hasMany('App\Module', 'course_id', 'id');
    }

    public static function getPorcentagem($val1, $val2){
        try {
            $value = ($val1*100) / $val2;
            return number_format((float)$value, 2, '.', '');
        } catch (\Throwable $th) {
            return 0;
        }
    }

    public static function checkResposta($val){
        if($val == 'correct'){
            return "CORRETO";
        }else if($val == 'incorrect'){
            return "INCORRETO";
        }
        return "";
    }

    public static function isOpen($start, $end){
        $now = new \DateTime('now');
        $start = new \DateTime($start);
        $end = new \DateTime($end);
        
        if( $now->getTimestamp() > $start->getTimestamp() && $now->getTimestamp() < $end->getTimestamp()){
            return true;
        }else{
            return false;
        }
    }

    public static function sqlFormularioComplementar($id){
        // PEGAR FORMULÁRIO COMPLEMENTAR
        $curso_frontend = \DB::connection('mysql_frontend')
            ->table("frontend_course")
            ->where("course_identifier", $id)
            ->first();

        // PEGAR ID DO QUIZ DO CURSO
        $quiz_id = \DB::connection('mysql_frontend')
            ->table("quiz_quiz_courses")
            ->where("course_id", $curso_frontend->uuid)
            ->first();

        if($quiz_id == null){
            return false;
        }else{
            $quiz_id = $quiz_id->quiz_id;
        }

        //PEGAR PERGUNTAS DO QUIZ
        $quiz_perguntas = \DB::connection('mysql_frontend')
            ->table("quiz_question")
            ->where("quiz_id", $quiz_id)
            ->get();

        //PEGAR ID DAS PERGUNTAS DO QUIZ
        $id_quiz_perguntas = \DB::connection('mysql_frontend')
            ->table("quiz_question")
            ->where("quiz_id", $quiz_id)
            ->get('uuid')->toArray();

        $id_quiz_perguntas_array = array();
        foreach($id_quiz_perguntas as $id_quiz_pergunta){
            $id_quiz_perguntas_array[] = $id_quiz_pergunta->uuid;
        }

        return $id_quiz_perguntas_array;
        
    }

    public static function getLink($link){
        try{
            // VERIFICAR SE ARQUIVO EXISTE
            $diskClound = \Storage::disk('s3');
            
            if($diskClound->exists("$link")){
              $url = $diskClound->temporaryUrl("$link", Carbon::now()->addMinutes(5));
              return $url;              
            }
        }catch (\Exception $e) {
            return "";
        } 
    }

}
