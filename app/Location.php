<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    //
    protected $connection = 'mysql_frontend';
    protected $table = 'address_city';
}
