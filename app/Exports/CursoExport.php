<?php

namespace App\Exports;

use App\Curso;
use App\Inscricao;
use App\Certificado;
use App\Module;
use App\Users;
use App\AvaliacaoItem;
use App\Question;
use Cache;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CursoExport implements FromCollection
{

    protected $id;

    function __construct($id) {
        $this->id = $id;
    }
    
    public function collection()
    {

        $curso = Curso::find($this->id); //PEGAR CURSO

        // PEGAR MATRICULAS
        $matriculas = Inscricao::where('course_id', $curso->id)
        ->whereNotIn(('user_id'), function($q){
            $q->select('user_id')->from('student_courseaccessrole');
        })->get();

        // PEGAR FORMULÁRIO COMPLEMENTAR
        $id_quiz_perguntas = $curso->sqlFormularioComplementar($this->id);
        $formulario_complementar = [];
        if(!empty($id_quiz_perguntas)){
            foreach($id_quiz_perguntas as $id_quiz_pergunta){
                $formulario_complementar[] = Question::where('uuid', $id_quiz_pergunta)->first()->title;
            }
        }

        // PEGAR IDS DAS AVALIAÇÕES
        $avaliacaos = AvaliacaoItem::where('course_id', $curso->id)->orderBy('order', 'asc')->get();

        // ESTRUTURAR MATRICULAS
        $items = array();
        foreach ($matriculas as $matricula)
        {

            // PEGAR RESPOSTAS DO FORMULARIO COMPLEMENTAR
            $formulario_complementar_resposta = [];
            if(isset($matricula->userFront->userQuiz)){
                foreach($matricula->userFront->userQuiz as $userQuiz){
                    if(!empty($id_quiz_perguntas)){
                        foreach($id_quiz_perguntas as $id_quiz_pergunta){
                            $formulario_complementar_resposta[] = $userQuiz->answer->where('question_id', $id_quiz_pergunta)->first()->content ?? '';
                        }
                    }
                }
            }

            // PEGAR RESPOSTAS DAS AVALIAÇÕES
            if(!empty($avaliacaos)){
                $respostas_avaliacaos = [];
                foreach($avaliacaos as $avaliacao){
                    $resposta = Module::where('student_id', $matricula->user_id)->where('module_id', $avaliacao->avaliacao_id)->first();
                    if(isset($resposta->state)){
                        $resposta = isset(json_decode($resposta->state)->correct_map) ? json_decode($resposta->state)->correct_map->{$avaliacao->avaliacao_item_id}->correctness : '';
                    }
                    $respostas_avaliacaos[] = $resposta;
                }
            }

            // CRIAR CABEÇALHO
            if(empty($items)){
                $header = [
                    'user_id',
                    'name',
                    'data_matricula',
                    'email',
                    'cpf',
                    'date',
                    'gender',
                    'uf',
                    'cidade',
                    'profissao',
                    'another_profession',
                    'level_formation',
                    'nota',
                ];
                foreach($formulario_complementar as $key => $fc){
                    $header = array_merge($header, [$fc => $fc ?? '']);
                }
                foreach($avaliacaos as $avaliacao){
                    array_push($header, $avaliacao->title);
                }
                $items[] = $header;
            }

            // PEGAR NOTA
            $nota = Certificado::where('user_id', $matricula->user_id)->where('course_id', $matricula->course_id)->first()->grade ?? '';

            // ESTRUTURAR MATRICULA
            $aluno = [
                'user_id' => $matricula->user_id,
                'name' => $matricula->userFront->name ?? '',
                'data_matricula' => $matricula->created ?? '',
                'email' => $matricula->user->email ?? '',
                'cpf' => $matricula->userFront->cpf ?? '',
                'date' => $matricula->userFront->date ?? '',
                'gender' => $matricula->userFront->gender ?? '',
                'uf' => $matricula->userFront->location->state_id ?? '',
                'cidade' => $matricula->userFront->location->name ?? '',
                'profissao' => $matricula->userFront->profession->name ?? '',
                'another_profession' => $matricula->userFront->another_profession ?? '',
                'level_formation' => $matricula->userFront->level_formation ?? '',
                'nota' => $nota
            ];

            foreach($formulario_complementar as $key => $fc){
                $aluno = array_merge($aluno, [$fc => $formulario_complementar_resposta[$key] ?? '']);
            }

            foreach($respostas_avaliacaos as $resposta_avaliacao){
                array_push($aluno, $resposta_avaliacao);
            }
            
            $items[] = $aluno;
        }

        $result = collect($items);
        
        return $result;
    }

}
