<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profession extends Model
{
    //
    protected $connection = 'mysql_frontend';
    protected $table = 'profession_profession';
}
