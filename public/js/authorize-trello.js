function AuthenticateTrello() {
    Trello.deauthorize();
    Trello.authorize({
        name: "Dashboard Trello",
        type: "modal",
        interactive: true,
        response_type: "token",
        expiration: "never",
        scope: { read: true },
        success: function () { onAuthorizeSuccessful(); },
        error: function () { onFailedAuthorization(); },
    });
 }

// APROVAR TOKEN DO USUÁRIO
function onAuthorizeSuccessful() {
    var token = Trello.token();
    console.log(token);
    atualizar_token_user(token);
}

function onFailedAuthorization() {
    console.log('error');
}

//VERIFICAR SE O USUÁRIO POSSUI TOKEN
AuthenticateTrello();

function atualizar_token_user(token){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $.ajax({
        type:"POST",
        url: "/trello/atualizar_token",
        data: {'token':token},
        beforeSend: function() {
        },
        success:function(data) {
            //TOKEN ATUALIZADO
            $.confirm({
                title: 'Sua conta foi vinculada com sucesso!',
                content: 'Agora você pode acompanhar os dados de seu Trello neste sistema',
                autoOpen: true,
                icon: 'fa fa-check',
                theme: 'modern',
                closeIcon: true,
                animation: 'scale',
                type: 'green',
                buttons: {
                    confirm: function () {
                        location.reload();
                    },
                    cancel: function () {
                        location.reload();
                    }
                }
            });
        },
        error:function(){
            // EERO AO ATUALIZAR O TOKEN//IMPRIMIR APROVADO
            $.confirm({
                title: 'Ocorreu algum problema ao atualizar seu TOken',
                content: 'Contate a assistência do sistema',
                buttons: {
                    confirm: function () {
                        location.reload();
                    },
                    cancel: function () {
                        location.reload();
                    }
                }
            });
        }
    });
};

