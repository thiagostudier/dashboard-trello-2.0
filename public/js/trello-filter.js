$( document ).ready(function() {
    
    filters = []
    // ESCONDER FILTROS SE ESTIVEREM NAS TELAS DE USUÁRIO OU ETIQUETA
    user = $("#id-member").html();
    label = $("#id-member").html();
    if(user){
        $(".filters").find("*[data-filter="+user+"]").remove();
        addFilter(user);
    }
    if(label){
        $(".filters").find("*[data-filter="+label+"]").remove();
        addFilter(label);
    }

    remove_cards();
    
    $(".filters .filter").click(function(){
        //PEGAR FILTRO
        filter = $(this).data("filter");
        // VERIFICAR SE O FILTRO ESTÁ ATIVO
        if( $(this).hasClass("active") ){
            // REMOVER FILTRO
            $(this).removeClass("active");
            removeFilter(filter);
        }else{
            // ADICIONAR FILTRO
            $(this).addClass("active");
            addFilter(filter);
        }
        filter_cards();
    });

    $(".date-filter").on("change", function(){
        from = $("#from").val();
        to = $("#to").val();
        filter_cards();
        atualizar_datas(from, to);
    });

    function addFilter(id){
        filters.push(id);
    }

    function removeFilter(id){
        filters.splice( filters.indexOf(id), 1 );
    }

    function filter_cards(){

        if ( !(typeof filters !== 'undefined' && filters.length > 0)) {
            check_date_on_load();
        }

        check = [];

        $(".row-card").each(function(index_1, item_1){
            element = this;
            filters.forEach(function(item_2, index_2){
                check.push("true");
                if( $(element).find("*[data-filter="+item_2+"]").length == 0 ){
                    check[index_1] = "false";
                }
            });
        });
        
        check.forEach(function(item_2, index_2){
            index_2 = index_2 + 1;
            check_card(item_2, index_2);
        });

        count_cards();
        count_cards_late();
        count_cards_finish();

    }

    filter_cards();

    function check_card(value, position){
        if((value == "true") && check_date($(".row-card:nth-child("+position+")").data("date")) ){
            $(".row-card:nth-child("+position+")").addClass("show");
            $(".row-card:nth-child("+position+")").removeClass("hide");
        }else{
            $(".row-card:nth-child("+position+")").addClass("hide");
            $(".row-card:nth-child("+position+")").removeClass("show");
        }
    }

    function check_date(check){
        from = $("#from").val();
        to = $("#to").val();
        if( (check <= to) && (check >= from) ){
            return true;
        } 
    }

    function check_date_on_load(){
        $(".row-card").each(function(index_1, item_1){
            element = this;
            date = $(element).data("date");
            if( check_date(date) ){
                $(element).addClass("show");
                $(element).removeClass("hide");
            }else{
                $(element).addClass("hide");
                $(element).removeClass("show");
            }
        });
    }

    check_date_on_load();

    function remove_cards(){
        filters.forEach(function(item){
            $(".row-card").each(function() {
                if( $(this).find("*[data-filter="+item+"]").length == 0 ){
                    $(this).remove();
                }
            });
        });
    }

    function count_cards(){
        var i = $('tbody tr:visible').length;
        $("#count-cards").html(i);
    }

    function count_cards_finish(){
        var i = $('tbody tr:visible .finish').length;
        $("#count-cards-finish").html(i);
    }

    function count_cards_late(){
        var i = $('tbody .is-late:visible').length;
        $("#count-cards-late").html(i);
    }

    function atualizar_datas(from, to){
        console.log("from: "+from);
        console.log("to: "+to);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });        
        $.ajax({
            type:"POST",
            url: "/trello/atualizar_datas",
            data: {'from':from,'to':to}
        });
    };

});