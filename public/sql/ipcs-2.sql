use edxapp;

SELECT 
u.id,
(select max(DATE_FORMAT(modified, '%d/%m/%Y'))
	from courseware_studentmodule
	where course_id = 'course-v1:Moinhos+TELEUTIP_PIPCSPTECI_2020+TELEUTIP_PIPCSPTECI_2020_T1' and student_id = u.id
	order by student_id, module_id) as 'ultimo_acesso',
DATE_FORMAT(m.created, '%d/%m/%Y') as data_matricula,
u.username,
up.name,
fr.name,
fr.cpf,
u.email,
DATE_FORMAT(fr.date, '%d/%m/%Y') as data_nascimento,
CASE fr.gender 
WHEN 1 THEN 'M' ELSE 'F' END AS 'sexo',
p.name as 'profissão',
fr.another_profession as 'outra profissão',
c.state_id as 'UF',
c.name as 'Cidade',
CASE fr.level_formation 
WHEN 5 THEN "Pós-Doutorado"
WHEN 10 THEN "Doutorado"
WHEN 20 THEN "Mestrado"
WHEN 30 THEN "Nível Superior completo - Bacharel"
WHEN 40 THEN "Nível Superior completo - Tecnólogo"
WHEN 50 THEN "Nível Superior incompleto - Bacharel"
WHEN 60 THEN "Nível Superior incompleto - Tecnólogo"
WHEN 70 THEN "Nível Técnico"
WHEN 80 THEN "Ensino Médio"
WHEN 90 THEN "Ensino Fundamental"
WHEN 99 THEN "Outro" END as 'Formação',

(SELECT qq.content FROM frontend.quiz_questionanswer qq INNER JOIN frontend.quiz_userquiz qu ON qu.uuid = qq.user_quiz_id  WHERE question_id = '0c8ac071167a4bc39cab8a241779b5cf' and qu.user_id = u.id) as 'Instituição/serviço de saúde',
(SELECT qq.content FROM frontend.quiz_questionanswer qq INNER JOIN frontend.quiz_userquiz qu ON qu.uuid = qq.user_quiz_id  WHERE question_id = 'c8f25c75251843589dd5efd45bf44c6f' and qu.user_id = u.id) as 'Setor que trabalha',

(SELECT json_unquote(state -> '$.correct_map."12642350230d4d76ba01904376f7dab1_2_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_PIPCSPTECI_2020+TELEUTIP_PIPCSPTECI_2020_T1+type@problem+block@12642350230d4d76ba01904376f7dab1' AND student_id = u.id) as 'pré-teste 1',
(SELECT json_unquote(state -> '$.correct_map."12642350230d4d76ba01904376f7dab1_3_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_PIPCSPTECI_2020+TELEUTIP_PIPCSPTECI_2020_T1+type@problem+block@12642350230d4d76ba01904376f7dab1' AND student_id = u.id) as 'pré-teste 2',
(SELECT json_unquote(state -> '$.correct_map."12642350230d4d76ba01904376f7dab1_4_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_PIPCSPTECI_2020+TELEUTIP_PIPCSPTECI_2020_T1+type@problem+block@12642350230d4d76ba01904376f7dab1' AND student_id = u.id) as 'pré-teste 3',

(SELECT json_unquote(state -> '$.correct_map."9aab70859faf42f88c0125cba9278d81_2_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_PIPCSPTECI_2020+TELEUTIP_PIPCSPTECI_2020_T1+type@problem+block@9aab70859faf42f88c0125cba9278d81' AND student_id = u.id) as 'pré-teste 4',
(SELECT json_unquote(state -> '$.correct_map."9aab70859faf42f88c0125cba9278d81_3_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_PIPCSPTECI_2020+TELEUTIP_PIPCSPTECI_2020_T1+type@problem+block@9aab70859faf42f88c0125cba9278d81' AND student_id = u.id) as 'pré-teste 5',
(SELECT json_unquote(state -> '$.correct_map."9aab70859faf42f88c0125cba9278d81_4_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_PIPCSPTECI_2020+TELEUTIP_PIPCSPTECI_2020_T1+type@problem+block@9aab70859faf42f88c0125cba9278d81' AND student_id = u.id) as 'pré-teste 6',

(SELECT json_unquote(state -> '$.correct_map."9eb19ba6d839463c9b35bf42d6d82eae_3_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_PIPCSPTECI_2020+TELEUTIP_PIPCSPTECI_2020_T1+type@problem+block@9eb19ba6d839463c9b35bf42d6d82eae' AND student_id = u.id) as 'pré-teste 7',
(SELECT json_unquote(state -> '$.correct_map."9eb19ba6d839463c9b35bf42d6d82eae_2_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_PIPCSPTECI_2020+TELEUTIP_PIPCSPTECI_2020_T1+type@problem+block@9eb19ba6d839463c9b35bf42d6d82eae' AND student_id = u.id) as 'pré-teste 8',
(SELECT json_unquote(state -> '$.correct_map."9eb19ba6d839463c9b35bf42d6d82eae_3_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_PIPCSPTECI_2020+TELEUTIP_PIPCSPTECI_2020_T1+type@problem+block@9eb19ba6d839463c9b35bf42d6d82eae' AND student_id = u.id) as 'pré-teste 9',
(SELECT json_unquote(state -> '$.correct_map."9eb19ba6d839463c9b35bf42d6d82eae_4_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_PIPCSPTECI_2020+TELEUTIP_PIPCSPTECI_2020_T1+type@problem+block@9eb19ba6d839463c9b35bf42d6d82eae' AND student_id = u.id) as 'pré-teste 10',

(SELECT json_unquote(state -> '$.correct_map."3a12962e82e44116afa8bc35985d2e87_2_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_PIPCSPTECI_2020+TELEUTIP_PIPCSPTECI_2020_T1+type@problem+block@3a12962e82e44116afa8bc35985d2e87' AND student_id = u.id) as 'pós-teste 1',
(SELECT json_unquote(state -> '$.correct_map."3a12962e82e44116afa8bc35985d2e87_3_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_PIPCSPTECI_2020+TELEUTIP_PIPCSPTECI_2020_T1+type@problem+block@3a12962e82e44116afa8bc35985d2e87' AND student_id = u.id) as 'pós-teste 2',
(SELECT json_unquote(state -> '$.correct_map."3a12962e82e44116afa8bc35985d2e87_4_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_PIPCSPTECI_2020+TELEUTIP_PIPCSPTECI_2020_T1+type@problem+block@3a12962e82e44116afa8bc35985d2e87' AND student_id = u.id) as 'pós-teste 3',
(SELECT json_unquote(state -> '$.correct_map."3a12962e82e44116afa8bc35985d2e87_5_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_PIPCSPTECI_2020+TELEUTIP_PIPCSPTECI_2020_T1+type@problem+block@3a12962e82e44116afa8bc35985d2e87' AND student_id = u.id) as 'pós-teste 4',
(SELECT json_unquote(state -> '$.correct_map."3a12962e82e44116afa8bc35985d2e87_6_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_PIPCSPTECI_2020+TELEUTIP_PIPCSPTECI_2020_T1+type@problem+block@3a12962e82e44116afa8bc35985d2e87' AND student_id = u.id) as 'pós-teste 5',
(SELECT json_unquote(state -> '$.correct_map."3a12962e82e44116afa8bc35985d2e87_7_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_PIPCSPTECI_2020+TELEUTIP_PIPCSPTECI_2020_T1+type@problem+block@3a12962e82e44116afa8bc35985d2e87' AND student_id = u.id) as 'pós-teste 6',
(SELECT json_unquote(state -> '$.correct_map."3a12962e82e44116afa8bc35985d2e87_8_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_PIPCSPTECI_2020+TELEUTIP_PIPCSPTECI_2020_T1+type@problem+block@3a12962e82e44116afa8bc35985d2e87' AND student_id = u.id) as 'pós-teste 7',
(SELECT json_unquote(state -> '$.correct_map."3a12962e82e44116afa8bc35985d2e87_9_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_PIPCSPTECI_2020+TELEUTIP_PIPCSPTECI_2020_T1+type@problem+block@3a12962e82e44116afa8bc35985d2e87' AND student_id = u.id) as 'pós-teste 8',
(SELECT json_unquote(state -> '$.correct_map."3a12962e82e44116afa8bc35985d2e87_10_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_PIPCSPTECI_2020+TELEUTIP_PIPCSPTECI_2020_T1+type@problem+block@3a12962e82e44116afa8bc35985d2e87' AND student_id = u.id) as 'pós-teste 9',
(SELECT json_unquote(state -> '$.correct_map."3a12962e82e44116afa8bc35985d2e87_11_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_PIPCSPTECI_2020+TELEUTIP_PIPCSPTECI_2020_T1+type@problem+block@3a12962e82e44116afa8bc35985d2e87' AND student_id = u.id) as 'pós-teste 10',

(SELECT grade FROM certificates_generatedcertificate WHERE user_id = u.id and course_id = m.course_id) as 'notas'
FROM student_courseenrollment m
INNER JOIN auth_user u ON u.id = m.user_id
INNER JOIN auth_userprofile up ON u.id = up.user_id
LEFT JOIN frontend.frontend_register fr ON u.id = fr.user_id
LEFT JOIN frontend.profession_profession p ON fr.profession_id = p.uuid
LEFT JOIN frontend.address_city c ON fr.city_id = c.id
WHERE
	u.id not in(
		SELECT distinct(user_id) 
		FROM student_courseaccessrole) AND
	course_id = 'course-v1:Moinhos+TELEUTIP_PIPCSPTECI_2020+TELEUTIP_PIPCSPTECI_2020_T1';