SELECT 
u.id,
(select max(DATE_FORMAT(modified, '%d/%m/%Y'))
	from courseware_studentmodule
	where course_id = 'course-v1:Moinhos+NTE_CAIEEC_2020+NTE_CAIEEC_2020_T1' and student_id = u.id
	order by student_id, module_id) as 'ultimo_acesso',
DATE_FORMAT(m.created, '%d/%m/%Y') as data_matricula,
u.username,
up.name,
fr.name,
fr.cpf,
u.email,
DATE_FORMAT(fr.date, '%d/%m/%Y') as data_nascimento,
CASE fr.gender 
WHEN 1 THEN 'M' ELSE 'F' END AS 'sexo',
p.name as 'profissão',
fr.another_profession as 'outra profissão',
c.state_id as 'UF',
c.name as 'Cidade',
CASE fr.level_formation 
WHEN 5 THEN "Pós-Doutorado"
WHEN 10 THEN "Doutorado"
WHEN 20 THEN "Mestrado"
WHEN 30 THEN "Nível Superior completo - Bacharel"
WHEN 40 THEN "Nível Superior completo - Tecnólogo"
WHEN 50 THEN "Nível Superior incompleto - Bacharel"
WHEN 60 THEN "Nível Superior incompleto - Tecnólogo"
WHEN 70 THEN "Nível Técnico"
WHEN 80 THEN "Ensino Médio"
WHEN 90 THEN "Ensino Fundamental"
WHEN 99 THEN "Outro" END as 'Formação',

(SELECT json_unquote(state -> '$.correct_map."36e15f151e3f45ab8d6fe12bd35f3245_2_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+NTE_CAIEEC_2020+NTE_CAIEEC_2020_T1+type@problem+block@36e15f151e3f45ab8d6fe12bd35f3245' AND student_id = u.id) as 'pré-teste 1',
(SELECT json_unquote(state -> '$.correct_map."36e15f151e3f45ab8d6fe12bd35f3245_3_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+NTE_CAIEEC_2020+NTE_CAIEEC_2020_T1+type@problem+block@36e15f151e3f45ab8d6fe12bd35f3245' AND student_id = u.id) as 'pré-teste 2',
(SELECT json_unquote(state -> '$.correct_map."36e15f151e3f45ab8d6fe12bd35f3245_4_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+NTE_CAIEEC_2020+NTE_CAIEEC_2020_T1+type@problem+block@36e15f151e3f45ab8d6fe12bd35f3245' AND student_id = u.id) as 'pré-teste 3',
(SELECT json_unquote(state -> '$.correct_map."36e15f151e3f45ab8d6fe12bd35f3245_5_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+NTE_CAIEEC_2020+NTE_CAIEEC_2020_T1+type@problem+block@36e15f151e3f45ab8d6fe12bd35f3245' AND student_id = u.id) as 'pré-teste 4',
(SELECT json_unquote(state -> '$.correct_map."36e15f151e3f45ab8d6fe12bd35f3245_6_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+NTE_CAIEEC_2020+NTE_CAIEEC_2020_T1+type@problem+block@36e15f151e3f45ab8d6fe12bd35f3245' AND student_id = u.id) as 'pré-teste 5',
(SELECT json_unquote(state -> '$.correct_map."36e15f151e3f45ab8d6fe12bd35f3245_7_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+NTE_CAIEEC_2020+NTE_CAIEEC_2020_T1+type@problem+block@36e15f151e3f45ab8d6fe12bd35f3245' AND student_id = u.id) as 'pré-teste 6',
(SELECT json_unquote(state -> '$.correct_map."36e15f151e3f45ab8d6fe12bd35f3245_8_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+NTE_CAIEEC_2020+NTE_CAIEEC_2020_T1+type@problem+block@36e15f151e3f45ab8d6fe12bd35f3245' AND student_id = u.id) as 'pré-teste 7',
(SELECT json_unquote(state -> '$.correct_map."36e15f151e3f45ab8d6fe12bd35f3245_9_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+NTE_CAIEEC_2020+NTE_CAIEEC_2020_T1+type@problem+block@36e15f151e3f45ab8d6fe12bd35f3245' AND student_id = u.id) as 'pré-teste 8',
(SELECT json_unquote(state -> '$.correct_map."36e15f151e3f45ab8d6fe12bd35f3245_10_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+NTE_CAIEEC_2020+NTE_CAIEEC_2020_T1+type@problem+block@36e15f151e3f45ab8d6fe12bd35f3245' AND student_id = u.id) as 'pré-teste 9',
(SELECT json_unquote(state -> '$.correct_map."36e15f151e3f45ab8d6fe12bd35f3245_11_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+NTE_CAIEEC_2020+NTE_CAIEEC_2020_T1+type@problem+block@36e15f151e3f45ab8d6fe12bd35f3245' AND student_id = u.id) as 'pré-teste 10',

(SELECT json_unquote(state -> '$.correct_map."a396418f09a54d3c8fd93c8a9f62a951_2_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+NTE_CAIEEC_2020+NTE_CAIEEC_2020_T1+type@problem+block@a396418f09a54d3c8fd93c8a9f62a951' AND student_id = u.id) as 'm2 - exercício 1',
(SELECT json_unquote(state -> '$.correct_map."a396418f09a54d3c8fd93c8a9f62a951_3_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+NTE_CAIEEC_2020+NTE_CAIEEC_2020_T1+type@problem+block@a396418f09a54d3c8fd93c8a9f62a951' AND student_id = u.id) as 'm2 - exercício 2',
(SELECT json_unquote(state -> '$.correct_map."a396418f09a54d3c8fd93c8a9f62a951_4_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+NTE_CAIEEC_2020+NTE_CAIEEC_2020_T1+type@problem+block@a396418f09a54d3c8fd93c8a9f62a951' AND student_id = u.id) as 'm2 - exercício 3.1',
(SELECT json_unquote(state -> '$.correct_map."a396418f09a54d3c8fd93c8a9f62a951_5_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+NTE_CAIEEC_2020+NTE_CAIEEC_2020_T1+type@problem+block@a396418f09a54d3c8fd93c8a9f62a951' AND student_id = u.id) as 'm2 - exercício 3.2',
(SELECT json_unquote(state -> '$.correct_map."a396418f09a54d3c8fd93c8a9f62a951_6_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+NTE_CAIEEC_2020+NTE_CAIEEC_2020_T1+type@problem+block@a396418f09a54d3c8fd93c8a9f62a951' AND student_id = u.id) as 'm2 - exercício 3.3',
(SELECT json_unquote(state -> '$.correct_map."a396418f09a54d3c8fd93c8a9f62a951_7_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+NTE_CAIEEC_2020+NTE_CAIEEC_2020_T1+type@problem+block@a396418f09a54d3c8fd93c8a9f62a951' AND student_id = u.id) as 'm2 - exercício 3.4',
(SELECT json_unquote(state -> '$.correct_map."a396418f09a54d3c8fd93c8a9f62a951_8_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+NTE_CAIEEC_2020+NTE_CAIEEC_2020_T1+type@problem+block@a396418f09a54d3c8fd93c8a9f62a951' AND student_id = u.id) as 'm2 - exercício 3.5',

(SELECT json_unquote(state -> '$.correct_map."41e49648c79047dc9b7363dc853263bb_2_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+NTE_CAIEEC_2020+NTE_CAIEEC_2020_T1+type@problem+block@41e49648c79047dc9b7363dc853263bb' AND student_id = u.id) as 'm4 - exercício 1.1',
(SELECT json_unquote(state -> '$.correct_map."6c5d7af94a744f07a1d2df40dfdfb0f6_2_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+NTE_CAIEEC_2020+NTE_CAIEEC_2020_T1+type@problem+block@6c5d7af94a744f07a1d2df40dfdfb0f6' AND student_id = u.id) as 'm4 - exercício 1.2',
(SELECT json_unquote(state -> '$.correct_map."8779daf03e9048f282237d15a53efb54_2_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+NTE_CAIEEC_2020+NTE_CAIEEC_2020_T1+type@problem+block@8779daf03e9048f282237d15a53efb54' AND student_id = u.id) as 'm4 - exercício 1.3',
(SELECT json_unquote(state -> '$.correct_map."1b9cd6a1358c4c7da77274bf400d9e79_2_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+NTE_CAIEEC_2020+NTE_CAIEEC_2020_T1+type@problem+block@1b9cd6a1358c4c7da77274bf400d9e79' AND student_id = u.id) as 'm4 - exercício 1.4',
(SELECT json_unquote(state -> '$.correct_map."6a7bc9718866494c960e5999780030c2_2_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+NTE_CAIEEC_2020+NTE_CAIEEC_2020_T1+type@problem+block@6a7bc9718866494c960e5999780030c2' AND student_id = u.id) as 'm4 - exercício 2.1',
(SELECT json_unquote(state -> '$.correct_map."6a7bc9718866494c960e5999780030c2_3_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+NTE_CAIEEC_2020+NTE_CAIEEC_2020_T1+type@problem+block@6a7bc9718866494c960e5999780030c2' AND student_id = u.id) as 'm4 - exercício 2.2',
(SELECT json_unquote(state -> '$.correct_map."6a7bc9718866494c960e5999780030c2_4_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+NTE_CAIEEC_2020+NTE_CAIEEC_2020_T1+type@problem+block@6a7bc9718866494c960e5999780030c2' AND student_id = u.id) as 'm4 - exercício 2.3',

(SELECT json_unquote(state -> '$.correct_map."75503360759540ee8931357ee0094512_2_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+NTE_CAIEEC_2020+NTE_CAIEEC_2020_T1+type@problem+block@75503360759540ee8931357ee0094512' AND student_id = u.id) as 'pós-teste 1',
(SELECT json_unquote(state -> '$.correct_map."75503360759540ee8931357ee0094512_3_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+NTE_CAIEEC_2020+NTE_CAIEEC_2020_T1+type@problem+block@75503360759540ee8931357ee0094512' AND student_id = u.id) as 'pós-teste 2',
(SELECT json_unquote(state -> '$.correct_map."75503360759540ee8931357ee0094512_4_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+NTE_CAIEEC_2020+NTE_CAIEEC_2020_T1+type@problem+block@75503360759540ee8931357ee0094512' AND student_id = u.id) as 'pós-teste 3',
(SELECT json_unquote(state -> '$.correct_map."75503360759540ee8931357ee0094512_5_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+NTE_CAIEEC_2020+NTE_CAIEEC_2020_T1+type@problem+block@75503360759540ee8931357ee0094512' AND student_id = u.id) as 'pós-teste 4',
(SELECT json_unquote(state -> '$.correct_map."75503360759540ee8931357ee0094512_6_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+NTE_CAIEEC_2020+NTE_CAIEEC_2020_T1+type@problem+block@75503360759540ee8931357ee0094512' AND student_id = u.id) as 'pós-teste 5',
(SELECT json_unquote(state -> '$.correct_map."75503360759540ee8931357ee0094512_7_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+NTE_CAIEEC_2020+NTE_CAIEEC_2020_T1+type@problem+block@75503360759540ee8931357ee0094512' AND student_id = u.id) as 'pós-teste 6',
(SELECT json_unquote(state -> '$.correct_map."75503360759540ee8931357ee0094512_8_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+NTE_CAIEEC_2020+NTE_CAIEEC_2020_T1+type@problem+block@75503360759540ee8931357ee0094512' AND student_id = u.id) as 'pós-teste 7',
(SELECT json_unquote(state -> '$.correct_map."75503360759540ee8931357ee0094512_9_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+NTE_CAIEEC_2020+NTE_CAIEEC_2020_T1+type@problem+block@75503360759540ee8931357ee0094512' AND student_id = u.id) as 'pós-teste 8',
(SELECT json_unquote(state -> '$.correct_map."75503360759540ee8931357ee0094512_10_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+NTE_CAIEEC_2020+NTE_CAIEEC_2020_T1+type@problem+block@75503360759540ee8931357ee0094512' AND student_id = u.id) as 'pós-teste 9',
(SELECT json_unquote(state -> '$.correct_map."75503360759540ee8931357ee0094512_11_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+NTE_CAIEEC_2020+NTE_CAIEEC_2020_T1+type@problem+block@75503360759540ee8931357ee0094512' AND student_id = u.id) as 'pós-teste 10'

FROM student_courseenrollment m
INNER JOIN auth_user u ON u.id = m.user_id
INNER JOIN auth_userprofile up ON u.id = up.user_id
LEFT JOIN frontend.frontend_register fr ON u.id = fr.user_id
LEFT JOIN frontend.profession_profession p ON fr.profession_id = p.uuid
LEFT JOIN frontend.address_city c ON fr.city_id = c.id
WHERE
	u.id not in(
		SELECT distinct(user_id) 
		FROM student_courseaccessrole) AND
	course_id = 'course-v1:Moinhos+NTE_CAIEEC_2020+NTE_CAIEEC_2020_T1'