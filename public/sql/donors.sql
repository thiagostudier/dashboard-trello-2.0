SELECT 
u.id,
(select max(DATE_FORMAT(modified, '%d/%m/%Y'))
	from courseware_studentmodule
	where course_id = 'course-v1:Moinhos+DNR_201901+DNR19_1' and student_id = u.id
	order by student_id, module_id) as 'ultimo_acesso',
DATE_FORMAT(m.created, '%d/%m/%Y') as data_matricula,
u.username,
up.name,
fr.name,
fr.cpf,
u.email,
DATE_FORMAT(fr.date, '%d/%m/%Y') as data_nascimento,
CASE fr.gender 
WHEN 1 THEN 'M' ELSE 'F' END AS 'sexo',
p.name as 'profissão',
fr.another_profession as 'outra profissão',
c.state_id as 'UF',
c.name as 'Cidade',
CASE fr.level_formation 
WHEN 5 THEN "Pós-Doutorado"
WHEN 10 THEN "Doutorado"
WHEN 20 THEN "Mestrado"
WHEN 30 THEN "Nível Superior completo - Bacharel"
WHEN 40 THEN "Nível Superior completo - Tecnólogo"
WHEN 50 THEN "Nível Superior incompleto - Bacharel"
WHEN 60 THEN "Nível Superior incompleto - Tecnólogo"
WHEN 70 THEN "Nível Técnico"
WHEN 80 THEN "Ensino Médio"
WHEN 90 THEN "Ensino Fundamental"
WHEN 99 THEN "Outro" END as 'Formação',

(SELECT qq.content FROM frontend.quiz_questionanswer qq INNER JOIN frontend.quiz_userquiz qu ON qu.uuid = qq.user_quiz_id  WHERE question_id = '672b0e7faaa7444383ec216fdd92d242' and qu.user_id = u.id) as 'instituição',
(SELECT qq.content FROM frontend.quiz_questionanswer qq INNER JOIN frontend.quiz_userquiz qu ON qu.uuid = qq.user_quiz_id  WHERE question_id = '20e1c68034094f9cb2898528b5af2279' and qu.user_id = u.id) as 'já trabalhou com doação de órgãos',
(SELECT qq.content FROM frontend.quiz_questionanswer qq INNER JOIN frontend.quiz_userquiz qu ON qu.uuid = qq.user_quiz_id  WHERE question_id = '828d9c91d9294eadb590f5be3113d058' and qu.user_id = u.id) as 'possui conhecimento sobre doação de órgãos',
(SELECT qq.content FROM frontend.quiz_questionanswer qq INNER JOIN frontend.quiz_userquiz qu ON qu.uuid = qq.user_quiz_id  WHERE question_id = '1d6cc38d04834c8dae2b948ba4e97c82' and qu.user_id = u.id) as 'trabalha em uti',
(SELECT qq.content FROM frontend.quiz_questionanswer qq INNER JOIN frontend.quiz_userquiz qu ON qu.uuid = qq.user_quiz_id  WHERE question_id = '8bf328dc299d453dac25c54044c94e37' and qu.user_id = u.id) as 'pertence à CIHDOTT/OPO',
(SELECT qq.content FROM frontend.quiz_questionanswer qq INNER JOIN frontend.quiz_userquiz qu ON qu.uuid = qq.user_quiz_id  WHERE question_id = '164b74d4a1474173a7aafc7b932c06b4' and qu.user_id = u.id) as 'realizou entrevista familiar para doação de órgãos',
(SELECT qq.content FROM frontend.quiz_questionanswer qq INNER JOIN frontend.quiz_userquiz qu ON qu.uuid = qq.user_quiz_id  WHERE question_id = '4d6a268e544f47c0b680afb71ef320f5' and qu.user_id = u.id) as 'recebeu alguma capacitação em comunicação em situações críticas',
(SELECT qq.content FROM frontend.quiz_questionanswer qq INNER JOIN frontend.quiz_userquiz qu ON qu.uuid = qq.user_quiz_id  WHERE question_id = '198a9baae8304956aab5fa29dfb333df' and qu.user_id = u.id) as ' recebeu alguma capacitação em entrevista familiar a potenciais doadores de órgãos',

(SELECT json_unquote(state -> '$.correct_map."1c3587bb896740a294d7ef9785671dd4_2_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@1c3587bb896740a294d7ef9785671dd4' AND student_id = u.id) as 'pré-teste 1',
(SELECT json_unquote(state -> '$.correct_map."1c3587bb896740a294d7ef9785671dd4_3_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@1c3587bb896740a294d7ef9785671dd4' AND student_id = u.id) as 'pré-teste 2',
(SELECT json_unquote(state -> '$.correct_map."1c3587bb896740a294d7ef9785671dd4_4_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@1c3587bb896740a294d7ef9785671dd4' AND student_id = u.id) as 'pré-teste 3',
(SELECT json_unquote(state -> '$.correct_map."1c3587bb896740a294d7ef9785671dd4_5_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@1c3587bb896740a294d7ef9785671dd4' AND student_id = u.id) as 'pré-teste 4',
(SELECT json_unquote(state -> '$.correct_map."1c3587bb896740a294d7ef9785671dd4_6_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@1c3587bb896740a294d7ef9785671dd4' AND student_id = u.id) as 'pré-teste 5',
(SELECT json_unquote(state -> '$.correct_map."1c3587bb896740a294d7ef9785671dd4_7_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@1c3587bb896740a294d7ef9785671dd4' AND student_id = u.id) as 'pré-teste 6',
(SELECT json_unquote(state -> '$.correct_map."1c3587bb896740a294d7ef9785671dd4_8_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@1c3587bb896740a294d7ef9785671dd4' AND student_id = u.id) as 'pré-teste 7',
(SELECT json_unquote(state -> '$.correct_map."1c3587bb896740a294d7ef9785671dd4_9_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@1c3587bb896740a294d7ef9785671dd4' AND student_id = u.id) as 'pré-teste 8',
(SELECT json_unquote(state -> '$.correct_map."1c3587bb896740a294d7ef9785671dd4_10_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@1c3587bb896740a294d7ef9785671dd4' AND student_id = u.id) as 'pré-teste 9',
(SELECT json_unquote(state -> '$.correct_map."1c3587bb896740a294d7ef9785671dd4_11_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@1c3587bb896740a294d7ef9785671dd4' AND student_id = u.id) as 'pré-teste 10',
(SELECT json_unquote(state -> '$.correct_map."1c3587bb896740a294d7ef9785671dd4_12_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@1c3587bb896740a294d7ef9785671dd4' AND student_id = u.id) as 'pré-teste 11',
(SELECT json_unquote(state -> '$.correct_map."1c3587bb896740a294d7ef9785671dd4_13_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@1c3587bb896740a294d7ef9785671dd4' AND student_id = u.id) as 'pré-teste 12',
(SELECT json_unquote(state -> '$.correct_map."1c3587bb896740a294d7ef9785671dd4_14_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@1c3587bb896740a294d7ef9785671dd4' AND student_id = u.id) as 'pré-teste 13',
(SELECT json_unquote(state -> '$.correct_map."1c3587bb896740a294d7ef9785671dd4_15_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@1c3587bb896740a294d7ef9785671dd4' AND student_id = u.id) as 'pré-teste 14',
(SELECT json_unquote(state -> '$.correct_map."1c3587bb896740a294d7ef9785671dd4_16_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@1c3587bb896740a294d7ef9785671dd4' AND student_id = u.id) as 'pré-teste 15',
(SELECT json_unquote(state -> '$.correct_map."1c3587bb896740a294d7ef9785671dd4_17_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@1c3587bb896740a294d7ef9785671dd4' AND student_id = u.id) as 'pré-teste 16',
(SELECT json_unquote(state -> '$.correct_map."1c3587bb896740a294d7ef9785671dd4_18_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@1c3587bb896740a294d7ef9785671dd4' AND student_id = u.id) as 'pré-teste 17',
(SELECT json_unquote(state -> '$.correct_map."1c3587bb896740a294d7ef9785671dd4_19_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@1c3587bb896740a294d7ef9785671dd4' AND student_id = u.id) as 'pré-teste 18',
(SELECT json_unquote(state -> '$.correct_map."1c3587bb896740a294d7ef9785671dd4_20_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@1c3587bb896740a294d7ef9785671dd4' AND student_id = u.id) as 'pré-teste 19',
(SELECT json_unquote(state -> '$.correct_map."1c3587bb896740a294d7ef9785671dd4_21_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@1c3587bb896740a294d7ef9785671dd4' AND student_id = u.id) as 'pré-teste 20',

(SELECT json_unquote(state -> '$.correct_map."6b539d88966d4f5a953961d2f7c9543b_2_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@6b539d88966d4f5a953961d2f7c9543b' AND student_id = u.id) as 'pós-teste 1',
(SELECT json_unquote(state -> '$.correct_map."6b539d88966d4f5a953961d2f7c9543b_3_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@6b539d88966d4f5a953961d2f7c9543b' AND student_id = u.id) as 'pós-teste 2',
(SELECT json_unquote(state -> '$.correct_map."6b539d88966d4f5a953961d2f7c9543b_4_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@6b539d88966d4f5a953961d2f7c9543b' AND student_id = u.id) as 'pós-teste 3',
(SELECT json_unquote(state -> '$.correct_map."6b539d88966d4f5a953961d2f7c9543b_5_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@6b539d88966d4f5a953961d2f7c9543b' AND student_id = u.id) as 'pós-teste 4',
(SELECT json_unquote(state -> '$.correct_map."6b539d88966d4f5a953961d2f7c9543b_6_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@6b539d88966d4f5a953961d2f7c9543b' AND student_id = u.id) as 'pós-teste 5',
(SELECT json_unquote(state -> '$.correct_map."6b539d88966d4f5a953961d2f7c9543b_7_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@6b539d88966d4f5a953961d2f7c9543b' AND student_id = u.id) as 'pós-teste 6',
(SELECT json_unquote(state -> '$.correct_map."6b539d88966d4f5a953961d2f7c9543b_8_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@6b539d88966d4f5a953961d2f7c9543b' AND student_id = u.id) as 'pós-teste 7',
(SELECT json_unquote(state -> '$.correct_map."6b539d88966d4f5a953961d2f7c9543b_9_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@6b539d88966d4f5a953961d2f7c9543b' AND student_id = u.id) as 'pós-teste 8',
(SELECT json_unquote(state -> '$.correct_map."6b539d88966d4f5a953961d2f7c9543b_10_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@6b539d88966d4f5a953961d2f7c9543b' AND student_id = u.id) as 'pós-teste 9',
(SELECT json_unquote(state -> '$.correct_map."6b539d88966d4f5a953961d2f7c9543b_11_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@6b539d88966d4f5a953961d2f7c9543b' AND student_id = u.id) as 'pós-teste 10',
(SELECT json_unquote(state -> '$.correct_map."6b539d88966d4f5a953961d2f7c9543b_12_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@6b539d88966d4f5a953961d2f7c9543b' AND student_id = u.id) as 'pós-teste 11',
(SELECT json_unquote(state -> '$.correct_map."6b539d88966d4f5a953961d2f7c9543b_13_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@6b539d88966d4f5a953961d2f7c9543b' AND student_id = u.id) as 'pós-teste 12',
(SELECT json_unquote(state -> '$.correct_map."6b539d88966d4f5a953961d2f7c9543b_14_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@6b539d88966d4f5a953961d2f7c9543b' AND student_id = u.id) as 'pós-teste 13',
(SELECT json_unquote(state -> '$.correct_map."6b539d88966d4f5a953961d2f7c9543b_15_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@6b539d88966d4f5a953961d2f7c9543b' AND student_id = u.id) as 'pós-teste 14',
(SELECT json_unquote(state -> '$.correct_map."6b539d88966d4f5a953961d2f7c9543b_16_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@6b539d88966d4f5a953961d2f7c9543b' AND student_id = u.id) as 'pós-teste 15',
(SELECT json_unquote(state -> '$.correct_map."6b539d88966d4f5a953961d2f7c9543b_17_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@6b539d88966d4f5a953961d2f7c9543b' AND student_id = u.id) as 'pós-teste 16',
(SELECT json_unquote(state -> '$.correct_map."6b539d88966d4f5a953961d2f7c9543b_18_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@6b539d88966d4f5a953961d2f7c9543b' AND student_id = u.id) as 'pós-teste 17',
(SELECT json_unquote(state -> '$.correct_map."6b539d88966d4f5a953961d2f7c9543b_19_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@6b539d88966d4f5a953961d2f7c9543b' AND student_id = u.id) as 'pós-teste 18',
(SELECT json_unquote(state -> '$.correct_map."6b539d88966d4f5a953961d2f7c9543b_20_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@6b539d88966d4f5a953961d2f7c9543b' AND student_id = u.id) as 'pós-teste 19',
(SELECT json_unquote(state -> '$.correct_map."6b539d88966d4f5a953961d2f7c9543b_21_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+DNR_201901+DNR19_1+type@problem+block@6b539d88966d4f5a953961d2f7c9543b' AND student_id = u.id) as 'pós-teste 20',

(SELECT grade FROM certificates_generatedcertificate WHERE user_id = u.id and course_id = m.course_id) as 'notas'
FROM student_courseenrollment m
INNER JOIN auth_user u ON u.id = m.user_id
INNER JOIN auth_userprofile up ON u.id = up.user_id
LEFT JOIN frontend.frontend_register fr ON u.id = fr.user_id
LEFT JOIN frontend.profession_profession p ON fr.profession_id = p.uuid
LEFT JOIN frontend.address_city c ON fr.city_id = c.id
WHERE
	u.id not in(
		SELECT distinct(user_id) 
		FROM student_courseaccessrole) AND
	course_id = 'course-v1:Moinhos+DNR_201901+DNR19_1'