use edxapp;

SELECT 
u.id,
(select max(DATE_FORMAT(modified, '%d/%m/%Y'))
	from courseware_studentmodule
	where course_id = 'course-v1:Moinhos+TELEUTIP_RPIPCSP_2020+TELEUTIP_RPIPCSP_2020_T1' and student_id = u.id
	order by student_id, module_id) as 'ultimo_acesso',
DATE_FORMAT(m.created, '%d/%m/%Y') as data_matricula,
u.username,
up.name,
fr.name,
fr.cpf,
u.email,
DATE_FORMAT(fr.date, '%d/%m/%Y') as data_nascimento,
CASE fr.gender 
WHEN 1 THEN 'M' ELSE 'F' END AS 'sexo',
p.name as 'profissão',
fr.another_profession as 'outra profissão',
c.state_id as 'UF',
c.name as 'Cidade',
CASE fr.level_formation 
WHEN 5 THEN "Pós-Doutorado"
WHEN 10 THEN "Doutorado"
WHEN 20 THEN "Mestrado"
WHEN 30 THEN "Nível Superior completo - Bacharel"
WHEN 40 THEN "Nível Superior completo - Tecnólogo"
WHEN 50 THEN "Nível Superior incompleto - Bacharel"
WHEN 60 THEN "Nível Superior incompleto - Tecnólogo"
WHEN 70 THEN "Nível Técnico"
WHEN 80 THEN "Ensino Médio"
WHEN 90 THEN "Ensino Fundamental"
WHEN 99 THEN "Outro" END as 'Formação',

(SELECT qq.content FROM frontend.quiz_questionanswer qq INNER JOIN frontend.quiz_userquiz qu ON qu.uuid = qq.user_quiz_id  WHERE question_id = '0c8ac071167a4bc39cab8a241779b5cf' and qu.user_id = u.id) as 'Instituição/serviço de saúde',
(SELECT qq.content FROM frontend.quiz_questionanswer qq INNER JOIN frontend.quiz_userquiz qu ON qu.uuid = qq.user_quiz_id  WHERE question_id = 'c8f25c75251843589dd5efd45bf44c6f' and qu.user_id = u.id) as 'Setor que trabalha',

(SELECT json_unquote(state -> '$.correct_map."d9b230643b4c4e0abfd031696b2aae81_2_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_RPIPCSP_2020+TELEUTIP_RPIPCSP_2020_T1+type@problem+block@d9b230643b4c4e0abfd031696b2aae81' AND student_id = u.id) as 'pré-teste 1',
(SELECT json_unquote(state -> '$.correct_map."d9b230643b4c4e0abfd031696b2aae81_3_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_RPIPCSP_2020+TELEUTIP_RPIPCSP_2020_T1+type@problem+block@d9b230643b4c4e0abfd031696b2aae81' AND student_id = u.id) as 'pré-teste 2',
(SELECT json_unquote(state -> '$.correct_map."d9b230643b4c4e0abfd031696b2aae81_4_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_RPIPCSP_2020+TELEUTIP_RPIPCSP_2020_T1+type@problem+block@d9b230643b4c4e0abfd031696b2aae81' AND student_id = u.id) as 'pré-teste 3',

(SELECT json_unquote(state -> '$.correct_map."98788ae491a24fbc9f3dea6c7e2c694d_2_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_RPIPCSP_2020+TELEUTIP_RPIPCSP_2020_T1+type@problem+block@98788ae491a24fbc9f3dea6c7e2c694d' AND student_id = u.id) as 'pré-teste 4',
(SELECT json_unquote(state -> '$.correct_map."98788ae491a24fbc9f3dea6c7e2c694d_3_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_RPIPCSP_2020+TELEUTIP_RPIPCSP_2020_T1+type@problem+block@98788ae491a24fbc9f3dea6c7e2c694d' AND student_id = u.id) as 'pré-teste 5',
(SELECT json_unquote(state -> '$.correct_map."98788ae491a24fbc9f3dea6c7e2c694d_4_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_RPIPCSP_2020+TELEUTIP_RPIPCSP_2020_T1+type@problem+block@98788ae491a24fbc9f3dea6c7e2c694d' AND student_id = u.id) as 'pré-teste 6',

(SELECT json_unquote(state -> '$.correct_map."43aafbaf0cdb4eb2a482f9cdb81276ed_3_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_RPIPCSP_2020+TELEUTIP_RPIPCSP_2020_T1+type@problem+block@43aafbaf0cdb4eb2a482f9cdb81276ed' AND student_id = u.id) as 'pré-teste 7',
(SELECT json_unquote(state -> '$.correct_map."43aafbaf0cdb4eb2a482f9cdb81276ed_2_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_RPIPCSP_2020+TELEUTIP_RPIPCSP_2020_T1+type@problem+block@43aafbaf0cdb4eb2a482f9cdb81276ed' AND student_id = u.id) as 'pré-teste 8',
(SELECT json_unquote(state -> '$.correct_map."43aafbaf0cdb4eb2a482f9cdb81276ed_3_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_RPIPCSP_2020+TELEUTIP_RPIPCSP_2020_T1+type@problem+block@43aafbaf0cdb4eb2a482f9cdb81276ed' AND student_id = u.id) as 'pré-teste 9',
(SELECT json_unquote(state -> '$.correct_map."43aafbaf0cdb4eb2a482f9cdb81276ed_4_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_RPIPCSP_2020+TELEUTIP_RPIPCSP_2020_T1+type@problem+block@43aafbaf0cdb4eb2a482f9cdb81276ed' AND student_id = u.id) as 'pré-teste 10',

(SELECT json_unquote(state -> '$.correct_map."12924eb3c47441fe82fe8b4044c6fdf9_2_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_RPIPCSP_2020+TELEUTIP_RPIPCSP_2020_T1+type@problem+block@12924eb3c47441fe82fe8b4044c6fdf9' AND student_id = u.id) as 'pós-teste 1',
(SELECT json_unquote(state -> '$.correct_map."12924eb3c47441fe82fe8b4044c6fdf9_3_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_RPIPCSP_2020+TELEUTIP_RPIPCSP_2020_T1+type@problem+block@12924eb3c47441fe82fe8b4044c6fdf9' AND student_id = u.id) as 'pós-teste 2',
(SELECT json_unquote(state -> '$.correct_map."12924eb3c47441fe82fe8b4044c6fdf9_4_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_RPIPCSP_2020+TELEUTIP_RPIPCSP_2020_T1+type@problem+block@12924eb3c47441fe82fe8b4044c6fdf9' AND student_id = u.id) as 'pós-teste 3',
(SELECT json_unquote(state -> '$.correct_map."12924eb3c47441fe82fe8b4044c6fdf9_5_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_RPIPCSP_2020+TELEUTIP_RPIPCSP_2020_T1+type@problem+block@12924eb3c47441fe82fe8b4044c6fdf9' AND student_id = u.id) as 'pós-teste 4',
(SELECT json_unquote(state -> '$.correct_map."12924eb3c47441fe82fe8b4044c6fdf9_6_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_RPIPCSP_2020+TELEUTIP_RPIPCSP_2020_T1+type@problem+block@12924eb3c47441fe82fe8b4044c6fdf9' AND student_id = u.id) as 'pós-teste 5',
(SELECT json_unquote(state -> '$.correct_map."12924eb3c47441fe82fe8b4044c6fdf9_7_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_RPIPCSP_2020+TELEUTIP_RPIPCSP_2020_T1+type@problem+block@12924eb3c47441fe82fe8b4044c6fdf9' AND student_id = u.id) as 'pós-teste 6',
(SELECT json_unquote(state -> '$.correct_map."12924eb3c47441fe82fe8b4044c6fdf9_8_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_RPIPCSP_2020+TELEUTIP_RPIPCSP_2020_T1+type@problem+block@12924eb3c47441fe82fe8b4044c6fdf9' AND student_id = u.id) as 'pós-teste 7',
(SELECT json_unquote(state -> '$.correct_map."12924eb3c47441fe82fe8b4044c6fdf9_9_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_RPIPCSP_2020+TELEUTIP_RPIPCSP_2020_T1+type@problem+block@12924eb3c47441fe82fe8b4044c6fdf9' AND student_id = u.id) as 'pós-teste 8',
(SELECT json_unquote(state -> '$.correct_map."12924eb3c47441fe82fe8b4044c6fdf9_10_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_RPIPCSP_2020+TELEUTIP_RPIPCSP_2020_T1+type@problem+block@12924eb3c47441fe82fe8b4044c6fdf9' AND student_id = u.id) as 'pós-teste 9',
(SELECT json_unquote(state -> '$.correct_map."12924eb3c47441fe82fe8b4044c6fdf9_11_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+TELEUTIP_RPIPCSP_2020+TELEUTIP_RPIPCSP_2020_T1+type@problem+block@12924eb3c47441fe82fe8b4044c6fdf9' AND student_id = u.id) as 'pós-teste 10',

(SELECT grade FROM certificates_generatedcertificate WHERE user_id = u.id and course_id = m.course_id) as 'notas'
FROM student_courseenrollment m
INNER JOIN auth_user u ON u.id = m.user_id
INNER JOIN auth_userprofile up ON u.id = up.user_id
LEFT JOIN frontend.frontend_register fr ON u.id = fr.user_id
LEFT JOIN frontend.profession_profession p ON fr.profession_id = p.uuid
LEFT JOIN frontend.address_city c ON fr.city_id = c.id
WHERE
	u.id not in(
		SELECT distinct(user_id) 
		FROM student_courseaccessrole) AND
	course_id = 'course-v1:Moinhos+TELEUTIP_RPIPCSP_2020+TELEUTIP_RPIPCSP_2020_T1';