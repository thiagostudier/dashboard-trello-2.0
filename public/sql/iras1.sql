SELECT 
u.id,
(select max(DATE_FORMAT(modified, '%d/%m/%Y'))
	from courseware_studentmodule
	where course_id = 'course-v1:Moinhos+Anvisa_2020_IRAS1+Anvisa_2020_IRAS1_T1' and student_id = u.id
	order by student_id, module_id) as 'ultimo_acesso',
DATE_FORMAT(m.created, '%d/%m/%Y') as data_matricula,
u.username,
up.name,
fr.name,
fr.cpf,
u.email,
DATE_FORMAT(fr.date, '%d/%m/%Y') as data_nascimento,
CASE fr.gender 
WHEN 1 THEN 'M' ELSE 'F' END AS 'sexo',
p.name as 'profissão',
fr.another_profession as 'outra profissão',
c.state_id as 'UF',
c.name as 'Cidade',
CASE fr.level_formation 
WHEN 5 THEN "Pós-Doutorado"
WHEN 10 THEN "Doutorado"
WHEN 20 THEN "Mestrado"
WHEN 30 THEN "Nível Superior completo - Bacharel"
WHEN 40 THEN "Nível Superior completo - Tecnólogo"
WHEN 50 THEN "Nível Superior incompleto - Bacharel"
WHEN 60 THEN "Nível Superior incompleto - Tecnólogo"
WHEN 70 THEN "Nível Técnico"
WHEN 80 THEN "Ensino Médio"
WHEN 90 THEN "Ensino Fundamental"
WHEN 99 THEN "Outro" END as 'Formação',

(SELECT qq.content FROM frontend.quiz_questionanswer qq INNER JOIN frontend.quiz_userquiz qu ON qu.uuid = qq.user_quiz_id  WHERE question_id = '1b6fdeb606184e52afd503060502a28b' and qu.user_id = u.id) as 'Profissional do Sistema Nacional de Vigilância Sanitária (SNVS)',
(SELECT qq.content FROM frontend.quiz_questionanswer qq INNER JOIN frontend.quiz_userquiz qu ON qu.uuid = qq.user_quiz_id  WHERE question_id = '2bf63964bbcd48de9c912201268e495f' and qu.user_id = u.id) as 'Atua em cargo de gestão no SNVS (gerente, coordenador, etc)?',
(SELECT qq.content FROM frontend.quiz_questionanswer qq INNER JOIN frontend.quiz_userquiz qu ON qu.uuid = qq.user_quiz_id  WHERE question_id = '423ed9aa85d948d0a84562688a26a869' and qu.user_id = u.id) as 'Formação',
(SELECT qq.content FROM frontend.quiz_questionanswer qq INNER JOIN frontend.quiz_userquiz qu ON qu.uuid = qq.user_quiz_id  WHERE question_id = '60129e1374b4424cb68a77e99f3e67c1' and qu.user_id = u.id) as 'Onde você trabalha?',
(SELECT qq.content FROM frontend.quiz_questionanswer qq INNER JOIN frontend.quiz_userquiz qu ON qu.uuid = qq.user_quiz_id  WHERE question_id = '74778508b509405da9a6dfdfcb810ff7' and qu.user_id = u.id) as 'Trabalha no SNVS há quanto tempo?',
(SELECT qq.content FROM frontend.quiz_questionanswer qq INNER JOIN frontend.quiz_userquiz qu ON qu.uuid = qq.user_quiz_id  WHERE question_id = '7d04d4935cf44b85a2246c39a775b6e5' and qu.user_id = u.id) as 'Você é estudante?',
(SELECT qq.content FROM frontend.quiz_questionanswer qq INNER JOIN frontend.quiz_userquiz qu ON qu.uuid = qq.user_quiz_id  WHERE question_id = 'e79e56f859f249efacb1704d9fc2499e' and qu.user_id = u.id) as 'Qual seu vínculo no SNVS?',

(SELECT json_unquote(state -> '$.correct_map."72fd38ca573044a69891b0d1a8b40495_2_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+Anvisa_2020_IRAS1+Anvisa_2020_IRAS1_T1+type@problem+block@72fd38ca573044a69891b0d1a8b40495' AND student_id = u.id) as 'pré-teste 1',
(SELECT json_unquote(state -> '$.correct_map."72fd38ca573044a69891b0d1a8b40495_3_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+Anvisa_2020_IRAS1+Anvisa_2020_IRAS1_T1+type@problem+block@72fd38ca573044a69891b0d1a8b40495' AND student_id = u.id) as 'pré-teste 2',
(SELECT json_unquote(state -> '$.correct_map."72fd38ca573044a69891b0d1a8b40495_4_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+Anvisa_2020_IRAS1+Anvisa_2020_IRAS1_T1+type@problem+block@72fd38ca573044a69891b0d1a8b40495' AND student_id = u.id) as 'pré-teste 3',
(SELECT json_unquote(state -> '$.correct_map."72fd38ca573044a69891b0d1a8b40495_5_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+Anvisa_2020_IRAS1+Anvisa_2020_IRAS1_T1+type@problem+block@72fd38ca573044a69891b0d1a8b40495' AND student_id = u.id) as 'pré-teste 4',
(SELECT json_unquote(state -> '$.correct_map."72fd38ca573044a69891b0d1a8b40495_6_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+Anvisa_2020_IRAS1+Anvisa_2020_IRAS1_T1+type@problem+block@72fd38ca573044a69891b0d1a8b40495' AND student_id = u.id) as 'pré-teste 5',
(SELECT json_unquote(state -> '$.correct_map."72fd38ca573044a69891b0d1a8b40495_7_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+Anvisa_2020_IRAS1+Anvisa_2020_IRAS1_T1+type@problem+block@72fd38ca573044a69891b0d1a8b40495' AND student_id = u.id) as 'pré-teste 6',
(SELECT json_unquote(state -> '$.correct_map."72fd38ca573044a69891b0d1a8b40495_8_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+Anvisa_2020_IRAS1+Anvisa_2020_IRAS1_T1+type@problem+block@72fd38ca573044a69891b0d1a8b40495' AND student_id = u.id) as 'pré-teste 7',
(SELECT json_unquote(state -> '$.correct_map."72fd38ca573044a69891b0d1a8b40495_9_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+Anvisa_2020_IRAS1+Anvisa_2020_IRAS1_T1+type@problem+block@72fd38ca573044a69891b0d1a8b40495' AND student_id = u.id) as 'pré-teste 8',
(SELECT json_unquote(state -> '$.correct_map."72fd38ca573044a69891b0d1a8b40495_10_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+Anvisa_2020_IRAS1+Anvisa_2020_IRAS1_T1+type@problem+block@72fd38ca573044a69891b0d1a8b40495' AND student_id = u.id) as 'pré-teste 9',
(SELECT json_unquote(state -> '$.correct_map."72fd38ca573044a69891b0d1a8b40495_11_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+Anvisa_2020_IRAS1+Anvisa_2020_IRAS1_T1+type@problem+block@72fd38ca573044a69891b0d1a8b40495' AND student_id = u.id) as 'pré-teste 10',

(SELECT json_unquote(state -> '$.correct_map."172d27a7c54c4733872d680b31e5d586_2_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+Anvisa_2020_IRAS1+Anvisa_2020_IRAS1_T1+type@problem+block@172d27a7c54c4733872d680b31e5d586' AND student_id = u.id) as 'pós-teste 1',
(SELECT json_unquote(state -> '$.correct_map."172d27a7c54c4733872d680b31e5d586_3_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+Anvisa_2020_IRAS1+Anvisa_2020_IRAS1_T1+type@problem+block@172d27a7c54c4733872d680b31e5d586' AND student_id = u.id) as 'pós-teste 2',
(SELECT json_unquote(state -> '$.correct_map."172d27a7c54c4733872d680b31e5d586_4_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+Anvisa_2020_IRAS1+Anvisa_2020_IRAS1_T1+type@problem+block@172d27a7c54c4733872d680b31e5d586' AND student_id = u.id) as 'pós-teste 3',
(SELECT json_unquote(state -> '$.correct_map."172d27a7c54c4733872d680b31e5d586_5_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+Anvisa_2020_IRAS1+Anvisa_2020_IRAS1_T1+type@problem+block@172d27a7c54c4733872d680b31e5d586' AND student_id = u.id) as 'pós-teste 4',
(SELECT json_unquote(state -> '$.correct_map."172d27a7c54c4733872d680b31e5d586_6_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+Anvisa_2020_IRAS1+Anvisa_2020_IRAS1_T1+type@problem+block@172d27a7c54c4733872d680b31e5d586' AND student_id = u.id) as 'pós-teste 5',
(SELECT json_unquote(state -> '$.correct_map."172d27a7c54c4733872d680b31e5d586_7_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+Anvisa_2020_IRAS1+Anvisa_2020_IRAS1_T1+type@problem+block@172d27a7c54c4733872d680b31e5d586' AND student_id = u.id) as 'pós-teste 6',
(SELECT json_unquote(state -> '$.correct_map."172d27a7c54c4733872d680b31e5d586_8_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+Anvisa_2020_IRAS1+Anvisa_2020_IRAS1_T1+type@problem+block@172d27a7c54c4733872d680b31e5d586' AND student_id = u.id) as 'pós-teste 7',
(SELECT json_unquote(state -> '$.correct_map."172d27a7c54c4733872d680b31e5d586_9_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+Anvisa_2020_IRAS1+Anvisa_2020_IRAS1_T1+type@problem+block@172d27a7c54c4733872d680b31e5d586' AND student_id = u.id) as 'pós-teste 8',
(SELECT json_unquote(state -> '$.correct_map."172d27a7c54c4733872d680b31e5d586_10_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+Anvisa_2020_IRAS1+Anvisa_2020_IRAS1_T1+type@problem+block@172d27a7c54c4733872d680b31e5d586' AND student_id = u.id) as 'pós-teste 9',
(SELECT json_unquote(state -> '$.correct_map."172d27a7c54c4733872d680b31e5d586_11_1".correctness') FROM courseware_studentmodule WHERE module_id = 'block-v1:Moinhos+Anvisa_2020_IRAS1+Anvisa_2020_IRAS1_T1+type@problem+block@172d27a7c54c4733872d680b31e5d586' AND student_id = u.id) as 'pós-teste 10',

(SELECT grade FROM certificates_generatedcertificate WHERE user_id = u.id and course_id = m.course_id) as 'notas'
FROM student_courseenrollment m
INNER JOIN auth_user u ON u.id = m.user_id
INNER JOIN auth_userprofile up ON u.id = up.user_id
LEFT JOIN frontend.frontend_register fr ON u.id = fr.user_id
LEFT JOIN frontend.profession_profession p ON fr.profession_id = p.uuid
LEFT JOIN frontend.address_city c ON fr.city_id = c.id
WHERE
	u.id not in(
		SELECT distinct(user_id) 
		FROM student_courseaccessrole) AND
	course_id = 'course-v1:Moinhos+Anvisa_2020_IRAS1+Anvisa_2020_IRAS1_T1'