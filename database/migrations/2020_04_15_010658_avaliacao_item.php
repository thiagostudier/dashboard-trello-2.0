<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AvaliacaoItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('avaliacao_item', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('course_id');
            $table->string('avaliacao_id');
            $table->string('avaliacao_item_id');
            $table->string('title');
            $table->integer('order');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
