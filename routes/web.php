<?php

Route::get('/', function(){
    return redirect('edx');
});

Route::get('/curso/{id}', 'EdxController@teste');
// EDX
Route::get('/edx', 'EdxController@index')->name('edx');
// RELATORIOS EDX
Route::get('/edx/curso/relatorio/alimentacao', 'EdxController@alimentacao')->name('alimentacao');
// CURSO
Route::get('/edx/curso/{id}', 'EdxController@curso')->name('curso');
// CURSO RELATÓRIO
Route::get('/edx/curso/relatorio/{id}', function($id){
    \App\Jobs\MakeExcelCursoComplete::dispatch($id);
    return redirect()->back();
})->name('curso_relatorio');
// PROJETOS
Route::get('/edx/projetos', 'EdxController@projetos')->name('projetos');
Route::get('/edx/projeto/{id}', 'EdxController@projeto')->name('projeto');

Auth::routes();
