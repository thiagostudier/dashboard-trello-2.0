<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Indicadores NTE') }}</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body>

    <div id="app">

        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">                
                <a class="navbar-brand" href="{{ route('trello') }}">
                    <img src="/img/trello-icon.png" width="30" height="30" class="d-inline-block align-top" alt="">
                    Indicadores
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item d-flex align-items-center mr-2">
                            <button data-toggle="collapse" data-target="#settings-trello" class="btn btn-light btn-border"><i class="fa fa-sliders" aria-hidden="true"></i></button>
                        </li>
                        <li class="nav-item d-flex align-items-center mr-2">
                            <form action="/trello/atualizar_board" method="POST">
                                <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                                <select class="form-control" id="change_board" name="id">
                                    @foreach($boards as $board)
                                        <option value="{{$board->id}}" <?= $board->id == $board_default ? 'selected' : '' ?> >{{$board->name}}</option>
                                    @endforeach
                                </select>
                            </form>
                        </li>
                        <li class="nav-item d-flex align-items-center mr-2">
                            <input type="date" value="{{$from}}" name="from" id="from" class="form-control date-filter" />
                        </li>
                        <li class="nav-item d-flex align-items-center mr-2">
                            <input type="date" value="{{$to}}" name="to" id="to" class="form-control date-filter" />
                        </li>
                        <li class="nav-item d-flex align-items-center">
                            <div class="list-card-members">
                                <a href="{{route('user', $member->id)}}" alt="Perfil">
                                    @if(!is_null($member->avatarUrl))
                                        <img class="member" src="{{$member->avatarUrl}}/50.png" title="{{$member->fullName}}" alt="{{$member->fullName}}"/>
                                    @else
                                        <h5 class="member">
                                            <span class="member-initials" title="{{$member->fullName}}">{{$member->initials}}</span>
                                        </h5>
                                    @endif
                                </a>
                            </div>
                            <span class="ml-2 text-truncate">{{$member->fullName}}</span>
                        </li>
                        <li class="nav-item d-flex align-items-center">
                            <a class="nav-link pl-4 pr-4" href="{{route('limparCache')}}" alt="Loggout">Sair</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="col-12 collapse shadow-sm" id="settings-trello">
            <div class="container pt-2 pb-2">
                
                <h5 class="text-center mb-3">Definições</h5>

                <form action="{{route('definition_create')}}" method="POST">
                    @csrf
                    <input type="hidden" name="board" value="{{$board_default}}">
                    <div class="row">
                        <div class="col-12 col-sm">
                            <h6>Lista de finalizado <i class="fa fa-info cursor-pointer" aria-hidden="true" data-toggle="tooltip" title="Defina uma lista como sendo a de atividades finalizadas"></i></h6>
                            @foreach($lists as $list)
                                <div class="checkbox">
                                    <label class="cursor-pointer"><input name="list_end" class="mr-2" type="radio" value="{{$list->id}}" <?=($list->id == $list_end) ? "checked='checked'" : "" ?>>{{$list->name}}</label>
                                </div>
                            @endforeach
                        </div>
                        <div class="col-12 col-sm">
                            <h6>Lista(s) para não contabilizar o tempo despedido <i class="fa fa-info cursor-pointer" aria-hidden="true" data-toggle="tooltip" title="Defina uma lista como sendo a de atividades finalizadas"></i></h6>
                            @foreach($lists as $list)
                                <div class="checkbox">
                                    <label class="cursor-pointer"><input name="list_not_count[]" class="mr-2" type="checkbox" value="{{$list->id}}" <?= in_array($list->id, $list_not_count) ? "checked='checked'" : '' ?>>{{$list->name}}</label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <input type="submit" class="btn btn-primary mr-auto d-block" value="Concluir definição" />
                        </div>
                    </div>
                </form>

            </div>
        </div>

        <main class="py-4">
            @yield('content')
        </main>

    </div>

    <script src="{{ asset('js/trello-functions.js') }}"></script>
    <script src="{{ asset('js/trello-filter.js') }}"></script>

    <script>
        
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });

    </script>

</body>
</html>