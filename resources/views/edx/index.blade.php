@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12 col-md-8">
            <h3><b>Cursos</b></h3>
        </div>
        <div class="col-12 col-md-4">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <h1 class="text-center mb-0"><b>{{$certificados_emitidos}}</b></h3>
                    <p class="text-center mb-0">Nº DE CERTIFICADOS</p>
                </div>
                <div class="col-12 col-sm-6">
                    <h1 class="text-center mb-0"><b>{{$inscricaos}}</b></h3>
                    <p class="text-center mb-0">Nº DE MATRÍCULAS</p>
                </div>
            </div>
        </div>
    </div>

    <table id="lista" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <td></td>
                <th>Curso</th>
                <th>Início</th>
                <th>Término</th>
                <th>Inscritos</th>
                <th>Certificados</th>
                <th>Inscritos X Certificados</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach($courses as $course)
                <tr>
                    <td><p>{{App\Curso::isOpen($course->start, $course->end) ? 'Aberto' : 'Fechado'}}</p></td>
                    <td>{{$course->display_name}}</td>
                    <td>{{Carbon\Carbon::parse($course->start)->format('d/m/Y')}}</td>
                    <td>{{Carbon\Carbon::parse($course->end)->format('d/m/Y')}}</td>
                    <td>{{count($course->inscricaos())}}</td>
                    <td>{{count($course->certificados())}}</td>
                    <td>{{App\Curso::getPorcentagem(count($course->certificados()),count($course->inscricaos()))}}%</td>
                    <td><a href="{{route('curso', $course->id)}}" class="btn btn-secondary" rel="noopener noreferrer"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                </tr>
            @endforeach
        </tbody>
        <tfooter>
            <tr>
                <td></td>
                <th scope="row">Total</th>
                <td></td>
                <td></td>
                <td><b>{{$inscricaos}}</b></td>
                <td><b>{{$certificados_emitidos}}</b></td>
                <td><b>{{App\Curso::getPorcentagem($certificados_emitidos,$inscricaos)}}%</b></td>
                <td></td>
            </tr>
        </tfooter>
    </table>
    
</div>
@endsection
