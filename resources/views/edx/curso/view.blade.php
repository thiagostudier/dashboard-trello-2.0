@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

        <div class="col-12 col-md-8">
            <h3><b>{{$curso->display_name}}</b></h3>
            <p><small>{{App\Curso::isOpen($curso->start, $curso->end) ? 'Aberto' : 'Fechado'}} - {{date('d/m/Y', strtotime($curso->start))}} - {{date('d/m/Y', strtotime($curso->end))}}</small></p>
        </div>

        <div class="col-12 col-md-4">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <h1 class="text-center mb-0"><b>{{$certificados_emitidos}}</b></h3>
                    <p class="text-center mb-0">Nº DE CERTIFICADOS</p>
                </div>
                <div class="col-12 col-sm-6">
                    <h1 class="text-center mb-0"><b>{{$inscricaos}}</b></h3>
                    <p class="text-center mb-0">Nº DE MATRÍCULAS</p>
                </div>
            </div>
        </div>

        <div class="col-12">
            <a class="btn btn-secondary d-inline-block mb-3" href="{{route('curso_relatorio', $curso->id)}}"><i class="fa fa-download" aria-hidden="true"></i> Baixar relatório</a>
            <p>Pode demorar uns minutos até o arquivo ser gerado.</p>
            <ol class="pl-3">
                @foreach($files as $file)
                <li><a href="{{$file['url']}}" target="_blank" rel="noopener noreferrer"><b>{{$file['name']['basename']}}</b></a></li>
                @endforeach
            </ol>
        </div>
        
    </div>
</div>

@endsection
