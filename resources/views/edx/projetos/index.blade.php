@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12 col-md-8">
            <h3><b>Projetos</b></h3>
        </div>       
    </div>

    <table id="lista" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Projeto</th>
                <th>Curriculos</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach($projetos as $projeto)
                <tr>
                    <td><p>{{$projeto->name}}</p></td>
                    <td><p>{{count($projeto->curriculos)}}</p></td>
                    <td><a href="{{route('projeto', $projeto->uuid)}}" class="btn btn-secondary" rel="noopener noreferrer"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                </tr>
            @endforeach
        </tbody>
        <tfooter>
            <tr>
                <td></td>
            </tr>
        </tfooter>
    </table>
    
</div>
@endsection
