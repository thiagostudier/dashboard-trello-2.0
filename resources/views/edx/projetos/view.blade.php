@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12 col-md-8">
            <h3><b>{{$projeto->name}}</b></h3>
        </div>       
    </div>

    <table id="lista" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Curso</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach($cursos as $curso)
                <tr>
                    <td><p>{{$curso->name}}</p></td>
                    <td><a href="{{route('curso', $curso->course_identifier)}}" class="btn btn-secondary" rel="noopener noreferrer"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                </tr>
            @endforeach
        </tbody>
        <tfooter>
            <tr>
                <td></td>
            </tr>
        </tfooter>
    </table>
    
</div>
@endsection
