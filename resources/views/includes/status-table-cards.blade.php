<div class="row">
    <div class="col-sm-6 col-12">
        <h6>Número de demandas: <b id="count-cards"></b></h6>
        <h6>Número de demandas finalizadas: <b id="count-cards-finish"></b> <span class="filter is-finish" data-filter="is-finish"><i class="fa fa-circle" aria-hidden="true"></i></span></h6>
        <h6>Número de demandas atrasadas: <b id="count-cards-late"></b> <span class="filter is-late" data-filter="is-late"><i class="fa fa-circle" aria-hidden="true"></i></span></h6>
    </div>
</div>