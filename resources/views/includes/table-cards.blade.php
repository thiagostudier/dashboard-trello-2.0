<?php
    use App\TrelloResources;
?>
<table id="table-cards" class="table table-hover">
    <thead class="thead-dark">
        <tr>
            <th scope="col">Tarefa</th>
            <th scope="col">Criado</th>
            <th scope="col">Entrega</th>
            <th scope="col">Lista</th>
            <th scope="col">Membros</th>
        </tr>
    </thead>
    <tbody>
        @foreach($cards as $card)
            <?php 
                $date_entrega = $card->badges->due ? \Carbon\Carbon::parse($card->badges->due)->format('d/m/y') : ''; // PEGAR DATA DE CRIAÇÃO
                $last_activy = ($card->dateLastActivity ? \Carbon\Carbon::parse($card->dateLastActivity)->format('d/m/y') : ''); // PEGAR DATA DA ÚLTIMA ATUALIZAÇÃO
            ?>
            <tr class="row-card <?=TrelloResources::cardIsLate($card)?>" data-date="{{TrelloResources::getDateCreateCardDefault($card->id)}}">
                <th scope="row {{$card->id}}">
                    <a href="{{route('card', $card->id)}}">{{$card->name}}</a>
                    @foreach($card->labels as $label)
                        <span class="badge badge-secondary small label" data-filter="{{$label->id}}">{{$label->name}}</span>
                    @endforeach
                    <a target="_blank" href="https://trello.com/c/{{$card->shortLink}}" title="Ver no Trello"><i class="fa fa-trello" aria-hidden="true"></i></a>
                </th>
                <td>
                    <span class="text-truncate small">{{TrelloResources::getDateCreateCard($card->id)}}</span>
                </td>
                <td>
                    <span class="text-truncate small" data-filter="<?=TrelloResources::cardIsLate($card)?>">
                        @if($date_entrega != '')
                            @if($card->badges->dueComplete)
                                <i class="fa fa-check-square-o finish" aria-hidden="true"></i>
                            @else
                                <i class="fa fa-square-o" aria-hidden="true"></i>
                            @endif
                            <b>{{$date_entrega}}</b>
                        @endif
                    </span>
                </td>
                <td>
                    <span class="text-truncate small">{{TrelloResources::getDataJson($lists, $card->idList)}} - <b>{{$last_activy}}</b></span>
                </td>
                <td>
                    <?php $values = $members->whereIn('id', $card->idMembers)->values()->all(); ?>
                    @include('includes.members_avatar')
                </td>
            </tr>
        @endforeach
    </tbody>
</table>