@foreach($values as $member)
    <div class="list-card-members filter member-item" data-filter="{{$member->id}}">
        <!-- <a href="{{route('user', $member->id)}}"> -->
        @if(!is_null($member->avatarUrl))
            <img class="member" src="{{$member->avatarUrl}}/50.png" title="{{$member->fullName}}" alt="{{$member->fullName}}"/>
        @else
            <h5 class="member">
                <span class="member-initials" title="{{$member->fullName}}">{{$member->initials}}</span>
            </h5>
        @endif
        <!-- </a> -->
    </div>
@endforeach