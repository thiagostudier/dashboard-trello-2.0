@extends('layouts.trello')

<?php

    use App\TrelloResources;

?>

@section('content')

<div class="container">

    <!-- TITULO DO CARD -->
    <div class="mb-2">        
        <h4><b>{{$card->name}}</b> <small>({{$list}})</small></h4>
    </div>

    <!-- ETIQUETAS -->
    <div class="mb-2">
        @foreach($labels as $label)
            <span class="mb-3 badge badge-secondary small filter" data-filter="{{$label->id}}">{{$label->name}}</span>
        @endforeach
    </div>

    <!-- PEGAR MEMBROS DO CARD -->
    <div class="d-flex mb-4">
        <?php $values = $members->whereIn('id', $card->idMembers)->values()->all(); ?>
        @include('includes.members_avatar')
    </div>

    <hr>
    <!-- TEMPO DESPEDIDO PARA A TAREFA -->
    <div class="mb-4">
        <h5><b>Tempo despedido</b></h5>
        <p class="mb-1"><b>Geral: </b>{{$time_until_finish['interval']}}</p>
        <p class="mb-1"><b>Período: </b>{{\Carbon\Carbon::parse($time_until_finish['date_create'])->format('d/m/y')}} - {{\Carbon\Carbon::parse($time_until_finish['date_end'])->format('d/m/y')}}</p>
        <!-- DATA DE CRIAÇÃO -->
        <p class="mb-1"><b>Data de Criação</b>: {{$date_create}}</p>
        <!-- DATA DE ENTREGA -->
        <p class="mb-1">
            <b>Data Prevista para Entrega</b>: {{$date_finish}}
            @if(isset($card->due) && $card->dueComplete)
                <i class="fa fa-check-square-o finish" aria-hidden="true"></i>
            @else
                <i class="fa fa-square-o" aria-hidden="true"></i>
            @endif
        </p>
        <!-- DATA DA ÚLTIMA ATIVIDADE -->
        <p class="mb-1"><b>Última Atividade</b>: {{$last_activy}}</p>
    </div>

    <hr>

    <!-- TEMPO QUE FICOU EM CADA LISTA -->
    <div class="mb-4">
        <h5><b>Tempo em cada Lista</b></h5>
        @foreach($time_on_lists as $time_on_list)
            <p class="mb-2"><b>{{$time_on_list['list']}}</b> {{$time_on_list['interval']}} ({{$time_on_list['date_create']}} - {{$time_on_list['date_end']}})</p>
        @endforeach
    </div>

    <hr>

    <div class="mb-4">
        <!-- DESCRIÇÃO -->
        <h5><b>Descrição</b></h5>
        <p>{{$card->desc}}</p>
    </div>

    @if(!empty($checklists))
        <div class="mb-4">
            <h5><b>Checklist</b></h5>
            <ul>
                @foreach($checklists as $checklist)
                    @foreach($checklist->checkItems as $checkItem)
                        <li><p>{{$checkItem->name}} <i class="fa <?= $checkItem->state == 'complete' ? 'fa-check-square-o' : 'fa-square-o' ?>" aria-hidden="true"></i>  </p></li>
                    @endforeach
                @endforeach
            </ul>
        </div>
    @endif


    <div>
        <!-- COMENTÁRIOS -->
        <h5 class="mb-4"><b>Comentários</b></h5>
        @foreach($actions as $action)
            @if($action->type == 'commentCard')
            <div class="list-card-members filter member-item" data-filter="{{$action->memberCreator->id}}">
                <!-- <a href="{{route('user', $member->id)}}"> -->
                @if(!is_null($action->memberCreator->avatarUrl))
                    <img class="member" src="{{$action->memberCreator->avatarUrl}}/50.png" title="{{$action->memberCreator->fullName}}" alt="{{$action->memberCreator->fullName}}"/>
                @else
                    <h5 class="member">
                        <span class="member-initials" title="{{$action->memberCreator->fullName}}">{{$member->initials}}</span>
                    </h5>
                @endif
                <!-- </a> -->
            </div>
            <p>{{$action->memberCreator->fullName}} <small>{{ \Carbon\Carbon::parse($action->date)->format('d/m/y h:m')}}</small></p>
            <p>{{$action->data->text}}</p>
            <br />
            @endif
        @endforeach
    </div>

</div>

<script src="{{ asset('js/trello-functions.js') }}"></script>

@endsection
