@extends('layouts.trello')
<?php
    use App\TrelloResources;
?>
@section('content')

<div class="container">
    <div class="row align-items-center mb-4">
        <div class="col-4 offset-4 offset-sm-0 col-sm-3 col-md-2">
            <div class="responsive-img rounded-circle overflow-hidden shadow">
                @if(!is_null($member_filter->avatarUrl))
                    <img src="{{$member_filter->avatarUrl}}/170.png" class="responsive-img-full" title="Usuário" />
                @else
                    <img src="\img\default-profile.jpg" class="responsive-img-full" title="Usuário" />
                @endif
            </div>
        </div>
        <div class="col-12 col-sm-9 col-md-10 filters">
            <span id="id-member" class="d-none">{{$member_filter->id}}</span>
            <h5><b>{{$member_filter->fullName}}</b></h5>
            @include('includes.status-table-cards')
        </div>
    </div>
    <div class="row mb-2 filters">
        <div class="col-12 col-md-3">
            <h6><b>LISTAS</b></h6>
            @include('includes.lists_form')
        </div>
        <div class="col-12 col-md-6">
            <h6><b>ETIQUETAS</b></h6>
            @foreach($labels as $label)
                <span class="badge badge-secondary small filter" data-filter="{{$label->id}}">{{$label->name}}</span>
            @endforeach
        </div>
        <div class="col-12 col-md-3">
            <h6><b>MEMBROS</b></h6>
            <?php $values = $members; ?>
            @include('includes.members_avatar')      
        </div>
    </div>
    @include('includes.table-cards')
</div>

@endsection
