@extends('layouts.trello')
@section('content')

<div class="container">
    <div class="row mb-2 filters">
        <div class="col-12">
            @include('includes.status-table-cards')
        </div>
    </div>
    <div class="row mb-2 filters">
        <div class="col-12 col-md-3">
            <h6><b>LISTAS</b></h6>
            @include('includes.lists_form')
        </div>
        <div class="col-12 col-md-6">
            <h6><b>ETIQUETAS</b></h6>
            @foreach($labels as $label)
                <span class="badge badge-secondary small filter" data-filter="{{$label->id}}">{{$label->name}}</span>
            @endforeach
        </div>
        <div class="col-12 col-md-3">
            <h6><b>MEMBROS</b></h6>
            <?php $values = $members; ?>
            @include('includes.members_avatar')  
        </div>
    </div>
    @include('includes.table-cards')
</div>

<script src="{{ asset('js/trello-functions.js') }}"></script>

@endsection
