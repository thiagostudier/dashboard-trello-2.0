@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col text-center">
            <h3>Autenticação do usuário no Trello</h3>
        </div>        
    </div>
    @if(!App\TrelloResources::hasToken())
        <script src="https://api.trello.com/1/client.js?key=f173ab582acacc165addb2dee7c21220" type="text/javascript"></script>
        <script src="{{ asset('js/authorize-trello.js') }}"></script>
    @endif
@endsection