require('./bootstrap');

import $ from 'jquery';
window.$ = window.jQuery = $;

import 'jquery/dist/jquery.js';

import 'jquery-ui-dist/jquery-ui.js';

$(document).ready(function() {
    
    $('#lista').DataTable({
        "order": [[ 1, 'asc' ]],
        "info":     false,
        "bJQueryUI": true,
        "oLanguage": {
            "sProcessing":   "Processando...",
            "sLengthMenu":   "Mostrar _MENU_ registros",
            "sZeroRecords":  "Não foram encontrados resultados",
            "sInfo":         "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty":    "Mostrando de 0 até 0 de 0 registros",
            "sInfoFiltered": "",
            "sInfoPostFix":  "",
            "sSearch":       "Buscar:",
            "sUrl":          "",
            "oPaginate": {
                "sFirst":    "Primeiro",
                "sPrevious": "Anterior",
                "sNext":     "Seguinte",
                "sLast":     "Último"
            }
        }
    });

    $('.table.table-striped').on('scroll', function (e) {
        $('.table.table-striped').scrollLeft();
    });

});
